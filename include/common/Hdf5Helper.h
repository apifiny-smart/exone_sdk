#pragma once
#include "H5Cpp.h"
using namespace H5;

template<typename T>
class DataBlock {
public:
	DataBlock(H5File* file, int nChunkRows, int ncols, const DataType& dtype, const string& name) {
		_nChunkRows = nChunkRows;
		_ncols = ncols;
		_dtype = dtype;
	    hsize_t dims[2] = {0, _ncols};
	    hsize_t chunkDims[2] = {_nChunkRows, _ncols};
	    hsize_t maxdims[2] = {H5S_UNLIMITED, H5S_UNLIMITED};
        _dataspace = new DataSpace(2, dims, maxdims);
        _prop = new DSetCreatPropList();
        _prop->setChunk(2, chunkDims);
        _dataset = new DataSet(file->createDataSet(H5std_string(name.c_str()), _dtype, *_dataspace, *_prop) );
	}

	~DataBlock() {
		close();
	}

	void allocRows(hsize_t nrows) {
		_nrows = nrows;
		hsize_t size[2], offset[2], dimsext[2];
		dimsext[0] = _nrows;
		dimsext[1] = _ncols;
		offset[0] = _total_rows;
		offset[1] = 0;
		size[0] = _total_rows + _nrows;
		size[1] = _ncols;

		_dataset->extend(size);

		if(_filespace != nullptr)
			delete _filespace;
		_filespace = new DataSpace(_dataset->getSpace());
		_filespace->selectHyperslab(H5S_SELECT_SET, dimsext, offset);

		if(_memspace != nullptr)
			delete _memspace;
		_memspace = new DataSpace(2, dimsext, NULL);

		if(_data != nullptr)
			delete[] _data;
        _data = new T[_nrows * _ncols];

        _total_rows += nrows;
        _pos = 0;
	}

	void appendData(const T* data, int len, bool alloc=false) {
		if(alloc)
			allocRows((len >= _ncols) ? len / _ncols : 1);
		memcpy(&_data[_pos], data, sizeof(T) * len);
		_pos += len;
		if(_pos >= _ncols * _nrows) {
			assert(_pos == _ncols * _nrows);
			_dataset->write(_data, _dtype, *_memspace, *_filespace);
		}
	}

	void close() {
		if(_prop != nullptr) {
			_prop->close();
			delete _prop;
		}
		if(_filespace != nullptr)
			delete _filespace;
		if(_memspace != nullptr)
			delete _memspace;
		if(_dataspace != nullptr)
			delete _dataspace;
		if(_data != nullptr)
			delete[] _data;
		if(_dataset != nullptr)
			delete _dataset;
	}

private:
	hsize_t _nChunkRows = 0;
	hsize_t _ncols = 0;
	hsize_t _nrows = 0;
	hsize_t _total_rows = 0;
	int _pos = 0;
	DataType _dtype;
	DSetCreatPropList* _prop = nullptr;
	DataSpace * _filespace = nullptr;
	DataSpace * _memspace = nullptr;
	DataSpace * _dataspace = nullptr;
	DataSet * _dataset = nullptr;
	T* _data = nullptr;
};

template<typename T>
class DataSeries {
public:
	DataSeries(H5File* file, hsize_t nChunkRows, const DataType& dtype, const string& name) {
		_nChunkRows = nChunkRows;
		_dtype = dtype;
	    hsize_t dims[1] = {0};
	    hsize_t chunkDims[1] = {_nChunkRows};
	    hsize_t maxdims[1] = {H5S_UNLIMITED};
        _dataspace = new DataSpace(1, dims, maxdims);
        _prop = new DSetCreatPropList();
        _prop->setChunk(1, chunkDims);
        _prop->setDeflate(6);
        _dataset = new DataSet(file->createDataSet(H5std_string(name.c_str()), _dtype, *_dataspace, *_prop) );
	}

	~DataSeries() {
		close();
	}

	void allocRows(int nrows) {
		_nrows = nrows;
		hsize_t size[1], offset[1], dimsext[1];
		dimsext[0] = _nrows;
		offset[0] = _total_rows;
		size[0] = _total_rows + _nrows;

		_dataset->extend(size);

		if(_filespace != nullptr)
			delete _filespace;
		_filespace = new DataSpace(_dataset->getSpace());
		_filespace->selectHyperslab(H5S_SELECT_SET, dimsext, offset);

		if(_memspace != nullptr)
			delete _memspace;
		_memspace = new DataSpace(1, dimsext, NULL);

		if(_data != nullptr)
			delete[] _data;
		_data = new T[_nrows];

        _total_rows += nrows;
        _pos = 0;
	}

	void appendData(const T* data, int len, bool alloc=false) {
		if(alloc)
			allocRows(len);
		memcpy(&_data[_pos], data, sizeof(T) * len);
		_pos += len;
		if(_pos >= _nrows) {
			assert(_pos == _nrows);
			_dataset->write(_data, _dtype, *_memspace, *_filespace);
		}
	}

	void appendData(const T data, bool alloc=false) {
		if(alloc)
			allocRows(1);
		_data[_pos] = data;
		_pos ++;
		if(_pos >= _nrows) {
			assert(_pos == _nrows);
			_dataset->write(_data, _dtype, *_memspace, *_filespace);
		}
	}

	void close() {
		if(_prop != nullptr) {
			_prop->close();
			delete _prop;
		}
		if(_filespace != nullptr)
			delete _filespace;
		if(_memspace != nullptr)
			delete _memspace;
		if(_dataspace != nullptr)
			delete _dataspace;
		if(_data != nullptr)
			delete[] _data;
		if(_dataset != nullptr)
			delete _dataset;
	}

private:
	hsize_t _nChunkRows = 0;
	hsize_t _nrows = 0;
	hsize_t _total_rows = 0;
	int _pos = 0;
	DataType _dtype;
	DSetCreatPropList* _prop = nullptr;
	DataSpace * _filespace = nullptr;
	DataSpace * _memspace = nullptr;
	DataSpace * _dataspace = nullptr;
	DataSet * _dataset = nullptr;
	T* _data = nullptr;
};
