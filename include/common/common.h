#pragma once
#include <memory>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <queue>
#include <algorithm>
#include <unordered_set>
#include <unordered_map>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <stdint.h>
#include <random>
#include <chrono>
#include "common/date/date.h"
#include "common/json_exone.hpp"
#include "common/json/json.h"
#include "common/logger.h"
#include "common/CsvReader.h"
#include <boost/algorithm/string.hpp>
#include <functional>

using namespace std;
using json = exone_nlohmann::json;
using namespace date;
using namespace std::chrono;

const double EPS = 1e-10; //1e-8;
const double DNAN = std::numeric_limits<double>::quiet_NaN();

typedef std::function<void()> VoidCB;
typedef void (*VoidFunc)();

template <class T>
T getJsonValue(json& obj, const string& key, T defvalue) {
	return (obj.find(key) == obj.end()) ? defvalue : obj.at(key).get<T>();
}

template <template<class,class,class...> class C, typename K, typename V, typename... Args>
V getWithDef(const C<K,V,Args...>& m, K const& key, const V & defval)
{
    typename C<K,V,Args...>::const_iterator it = m.find( key );
    if (it == m.end())
        return defval;
    return it->second;
}

//int sign(double v);
template <class T>
inline int sign(T v) {
    return (v > T(0)) - (v < T(0));
}

bool same_sign(double v1, double v2);
bool opposite_sign(double v1, double v2);
bool strict_same_sign(double v1, double v2);
bool strict_opposite_sign(double v1, double v2);
bool misceq(double x, double y);
bool miscge(double x, double y);
bool miscle(double x, double y);
bool miscgt(double x, double y);
bool misclt(double x, double y);
double smaller(double x, double y);

double f_round(double dval, int n);

long usecDiff(timeval& tv1, timeval& tv2);

int mkdirs(const string& dir);

nanoseconds now();
nanoseconds parseTime(int d, const string& t);
std::string timepointStr(const system_clock::time_point& t);
std::string toStr(const system_clock::time_point& t);
std::string toStr(const nanoseconds& nsecs);
hours utc_offset_Eastern_US(system_clock::time_point tp);
//system_clock::time_point to_local_timepoint(const nanoseconds& nsecs);
nanoseconds getMidnightNanoseconds(int date);
int getPreviousDate(int date);

json mergeConfig( const json &a, const json &b );
json loadConfig(const string& path,int date = 0);
bool loadBaseSymbolInfoConfig(const string& symbol,const string &exch,json &data,int date);

string doubleToString(double value);

std::string getExePath();
std::string getHomePath();
string doubleToString(double value,int accurate);
bool is_number(const std::string& s);
bool isDirectorExist(const string &pathname);