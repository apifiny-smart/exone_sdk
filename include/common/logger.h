#pragma once

#ifdef USE_ACE_LOG

#include <Logger.h>
//#define LOG_A(fmt, args...)
//#define LOG_E(fmt, args...)
//#define LOG_W(fmt, args...)
//#define LOG_I(fmt, args...)
//#define LOG_D(fmt, args...)
//#define LOG_T(fmt, args...)

#define QTS_LOG_A LOG_A
#define QTS_LOG_E LOG_E
#define QTS_LOG_W LOG_W
#define QTS_LOG_I LOG_I
#define QTS_LOG_D LOG_D
#define QTS_LOG_T LOG_T

#else

#define QTS_LOG4Z_FORMAT_INPUT_ENABLE
#include "common/log4z.h"

// see https://www.tutorialspoint.com/log4j/log4j_logging_levels.htm

#define QTS_OFF_LEVEL      0
#define QTS_ERROR_LEVEL    3
#define QTS_WARN_LEVEL     4
#define QTS_INFO_LEVEL     5
#define QTS_DEBUG_LEVEL    6
#define QTS_TRACE_LEVEL    7
#define QTS_ALL_LEVEL      8


//this option is optional, but can be used by application to optimize the logger
//at compile time. If not set, it should be ALL_LEVEL so log4z's config has full control.
#ifndef QTS_LOG_LEVEL
#define QTS_LOG_LEVEL   QTS_ALL_LEVEL
#endif

#define QTS_LOG_A(fmt, ...)     QTS_LOGFMT_ALWAYS(qts::log4z::LOG4Z_MAIN_LOGGER_ID, fmt,  ##__VA_ARGS__)

#if QTS_LOG_LEVEL >= QTS_ERROR_LEVEL
#define QTS_LOG_E(fmt, ...)     QTS_LOGFMT_ERROR(qts::log4z::LOG4Z_MAIN_LOGGER_ID, fmt,  ##__VA_ARGS__)
#else
#define QTS_LOG_E(fmt, args...)
#endif


#if QTS_LOG_LEVEL >= QTS_WARN_LEVEL
#define QTS_LOG_W(fmt, ...)     QTS_LOGFMT_WARN(qts::log4z::LOG4Z_MAIN_LOGGER_ID, fmt,  ##__VA_ARGS__)
#else
#define QTS_LOG_W(fmt, args...)
#endif


#if QTS_LOG_LEVEL >= QTS_INFO_LEVEL
#define QTS_LOG_I(fmt, ...)     QTS_LOGFMT_INFO(qts::log4z::LOG4Z_MAIN_LOGGER_ID, fmt,  ##__VA_ARGS__)
#else
#define QTS_LOG_I(fmt, args...)
#endif

#if QTS_LOG_LEVEL >= QTS_DEBUG_LEVEL
#define QTS_LOG_D(fmt, ...)     QTS_LOGFMT_DEBUG(qts::log4z::LOG4Z_MAIN_LOGGER_ID, fmt,  ##__VA_ARGS__)
#else
#define QTS_LOG_D(fmt, args...)
#endif

#if QTS_LOG_LEVEL >= QTS_TRACE_LEVEL
#define QTS_LOG_T(fmt, ...)     QTS_LOGFMT_TRACE(qts::log4z::LOG4Z_MAIN_LOGGER_ID, fmt,  ##__VA_ARGS__)
#else
#define QTS_LOG_T(fmt, args...)
#endif

#define QTS_SIM_LOG_START(log_level, path) \
qts::log4z::ILog4zManager::getRef().setLoggerPath(qts::log4z::LOG4Z_MAIN_LOGGER_ID, path.c_str()); \
qts::log4z::ILog4zManager::getRef().setLoggerLevel(qts::log4z::LOG4Z_MAIN_LOGGER_ID, log_level); \
qts::log4z::ILog4zManager::getRef().setLoggerDisplay(qts::log4z::LOG4Z_MAIN_LOGGER_ID, false); \
qts::log4z::ILog4zManager::getRef().setLoggerOutFile(qts::log4z::LOG4Z_MAIN_LOGGER_ID, true); \
qts::log4z::ILog4zManager::getRef().setLoggerFileLine(qts::log4z::LOG4Z_MAIN_LOGGER_ID, false); \
qts::log4z::ILog4zManager::getRef().start()

#define QTS_LOG_START(log_level, path) \
qts::log4z::ILog4zManager::getRef().setLoggerPath(qts::log4z::LOG4Z_MAIN_LOGGER_ID, path.c_str()); \
qts::log4z::ILog4zManager::getRef().setLoggerLevel(qts::log4z::LOG4Z_MAIN_LOGGER_ID, log_level); \
qts::log4z::ILog4zManager::getRef().setLoggerDisplay(qts::log4z::LOG4Z_MAIN_LOGGER_ID, false); \
qts::log4z::ILog4zManager::getRef().setLoggerOutFile(qts::log4z::LOG4Z_MAIN_LOGGER_ID, true); \
qts::log4z::ILog4zManager::getRef().setLoggerFileLine(qts::log4z::LOG4Z_MAIN_LOGGER_ID, false); \
qts::log4z::ILog4zManager::getRef().start()

#endif
