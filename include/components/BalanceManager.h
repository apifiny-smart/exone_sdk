#pragma once
#include "common/common.h"

struct BalanceItem {
	double balance = 0.0;
};
typedef unordered_map<string, BalanceItem* > BalanceMap;

class BalanceManager {
public:
	/**
	 * @brief get the balance map
	 * 
	 * @return BalanceMap& 
	 */
	BalanceMap& balanceMap() { return _balanceMap; }
	/**
	 * @brief Get the Balance Item object
	 * 
	 * @param itemName 
	 * @return BalanceItem* 
	 */
	BalanceItem* getBalanceItem(const string& itemName);

public:
	static BalanceManager* g_instance;
	static BalanceManager* instance();

private:
	BalanceMap _balanceMap;
};
