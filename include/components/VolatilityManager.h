#pragma once
#include "common/common.h"

struct VolatilityItem {
	double value = 0.0;
};
typedef unordered_map<string, VolatilityItem* > VolatilityMap;

class VolatilityManager {
public:
	VolatilityMap& volatilityMap() { return _volatilityMap; }
	VolatilityItem* getVolatilityItem(const string& itemName);

public:
	static VolatilityManager* g_instance;
	static VolatilityManager* instance();

private:
	VolatilityMap _volatilityMap;
};
