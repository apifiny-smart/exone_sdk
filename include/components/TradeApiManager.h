#pragma once
#include "common/common.h"
#include "base/TradeApi.h"

class TradeApiManager {
public:
	TradeApiManager() {};
	/**
	 * @brief initial
	 * 
	 */
	void init();
	/**
	 * @brief Set the Trade Api object
	 * 
	 * @param tradeApi 
	 */
	void setTradeApi(TradeApi* tradeApi) { _tradeApi = tradeApi; }
	/**
	 * @brief get the Trade Api object
	 * 
	 * @return TradeApi* 
	 */
	TradeApi* tradeApi() { return _tradeApi; }
public:
	static TradeApiManager* g_instance;
	static TradeApiManager* instance();

private:
	TradeApi* _tradeApi;
};
