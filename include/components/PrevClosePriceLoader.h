#pragma once
#include "base/Player.h"
#include "common/common.h"

using namespace std;

class PrevClosePriceLoader {
public:
    PrevClosePriceLoader(const string& pdir, int date);
    double getPrevClosePrice(const string& ticker, const string& exch);
    int getCalendarDays() { return _calendarDays; }

protected:
    string _pdir;
    int _date;
    int _calendarDays = 1;
    unordered_map<string, double> _pxmap;
};