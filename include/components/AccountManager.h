#pragma once
#include "base/Account.h"

typedef unordered_map<long, Account*> AccountMap;

class AccountManager {
public:
	void load(json& cfg);
	void init();
	AccountMap& accountMap() { return _accountMap; }
	Account* getAccount(long acctId);
	//Account* addAccount(long acctId);
	Account* addAccount(long acctId, Account* acct);

public:
	static AccountManager* g_instance;
	static AccountManager* instance();

private:
	AccountMap _accountMap;
};
