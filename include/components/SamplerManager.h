#pragma once
#include "base/Sampler.h"
#include "base/utils.h"
#include "base/XlibComponentFactory.h"

using namespace std;

typedef unordered_map<string, Sampler*> SamplerMap;

class SamplerManager {
public:
	SamplerManager();//done
	/**
	 * @brief add XlibComponentFactory
	 * 
	 * @param xmgr 
	 */
	void addXlibComponentFactory(XlibComponentFactoryPtr xmgr);
	/**
	 * @brief load config json
	 * 
	 * @param cfg 
	 */
	void load(json& cfg);//done
	/**
	 * @brief initial
	 * 
	 */
	void init();//done
	/**
	 * @brief get the sampler map
	 * 
	 * @return SamplerMap& 
	 */
	SamplerMap& samplerMap() { return _samplerMap; }//done
	/**
	 * @brief Get the Sampler object
	 * 
	 * @param name 
	 * @return Sampler* 
	 */
	Sampler* getSampler(const string& name);//done
	/**
	 * @brief add sampler
	 * 
	 * @param name 
	 * @param sampler 
	 */
	void addSampler(const string& name, Sampler* sampler);//done

public:
	static SamplerManager* g_instance;//done
	static SamplerManager* instance();//done

private:
	map<string,XlibComponentFactoryPtr> _xmgrs;//什么作用？
	SamplerMap _samplerMap;//done
	typedef unordered_map<string, string> AliasMap;  //??
	AliasMap _aliasMap; //??
};

