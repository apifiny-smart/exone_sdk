#pragma once
#include "base/PricingModel.h"
#include "base/utils.h"
#include "base/XlibComponentFactory.h"

using namespace std;

typedef unordered_map<string, PricingModel*> PricingModelMap;

class PricingModelManager {
public:
	PricingModelManager();
	/**
	 * @brief add XlibComponentFactory
	 * 
	 * @param xmgr 
	 */
	void addXlibComponentFactory(XlibComponentFactoryPtr xmgr);
	/**
	 * @brief load the config json
	 * 
	 * @param cfg 
	 */
	void load(json& cfg);
	/**
	 * @brief initial
	 * 
	 */
	void init();
	/**
	 * @brief get the pricingmodelmap
	 * 
	 * @return PricingModelMap& 
	 */
	PricingModelMap& pricingModelMap() { return _pmMap; }
	/**
	 * @brief Get the Pricing Model object
	 * 
	 * @param name 
	 * @return PricingModel* 
	 */
	PricingModel* getPricingModel(const string& name);
	/**
	 * @brief add Pricing Model object
	 * 
	 * @param name 
	 * @param attr 
	 */
	void addPricingModel(const string& name, json& attr);
	/**
	 * @brief add Pricing Model object
	 * 
	 * @param name 
	 * @param pm 
	 */
	void addPricingModel(const string& name, PricingModel* pm);
	//PricingModel* addPricingModel(const string& pmType, const string& ticker, const string& exch, const string& name="");

public:
	static PricingModelManager* g_instance;
	static PricingModelManager* instance();

private:
	map<string,XlibComponentFactoryPtr> _xmgrs;
	PricingModelMap _pmMap;
	typedef unordered_map<string, string> AliasMap;
	AliasMap _aliasMap;
};
