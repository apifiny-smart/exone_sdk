#pragma once
#include "base/Model.h"
#include "base/utils.h"
#include "base/XlibComponentFactory.h"

using namespace std;

typedef unordered_map<string, Model*> ModelMap;

class ModelManager {
public:
	ModelManager();
	void addXlibComponentFactory(XlibComponentFactoryPtr xmgr);
	void load(json& cfg);
	void init();
	ModelMap& modelMap() { return _modelMap; }
	Model* getModel(const string& name);
	void addModel(const string& name, Model* model);

public:
	static ModelManager* g_instance;
	static ModelManager* instance();

private:
	ModelMap _modelMap;
	map<string,XlibComponentFactoryPtr> _xmgrs;

};
