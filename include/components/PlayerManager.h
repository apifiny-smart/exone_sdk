#pragma once
#include "base/Player.h"
#include "common/common.h"

using namespace std;

typedef unordered_map<string, Player*> PlayerMap;

class PlayerManager {
public:
	PlayerManager();
	/**
	 * @brief load config json
	 * 
	 * @param cfg 
	 * @param tradeDate 
	 */
	void load(json& cfg, int tradeDate);
	/**
	 * @brief initial
	 * 
	 */
	void init();
	/**
	 * @brief close
	 * 
	 */
	void close();
	/**
	 * @brief get the player map
	 * 
	 * @return PlayerMap& 
	 */
	PlayerMap& playerMap() { return _playerMap; }
	/**
	 * @brief Get the Player object
	 * 
	 * @param name 
	 * @return Player* 
	 */
	Player* getPlayer(const string& name);
	/**
	 * @brief add the player
	 * 
	 * @param name 
	 * @param player 
	 */
	void addPlayer(const string& name, Player* player);

public:
	static PlayerManager* g_instance;
	static PlayerManager* instance();

private:
	PlayerMap _playerMap;
};
