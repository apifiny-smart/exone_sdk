#pragma once
#include "common/common.h"

class ConfigManager {
public:
	/**
	 * @brief Construct a new Config Manager object
	 * 
	 */
	ConfigManager() {};
	/**
	 * @brief initial config
	 * 
	 */
	void init() {}
	/**
	 * @brief Set the Json Cfg object
	 * 
	 * @param cfg 
	 */
	void setJsonCfg(json& cfg) { _cfg = cfg; }
	/**
	 * @brief get the Json Cfg object
	 * 
	 * @return json& 
	 */
	json& jsonCfg() { return _cfg; }
public:
	static ConfigManager* g_instance;
	static ConfigManager* instance();

private:
	json _cfg;
};
