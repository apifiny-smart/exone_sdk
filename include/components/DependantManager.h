#pragma once
#include "base/utils.h"
#include "base/Dependant.h"
#include "base/XlibComponentFactory.h"

using namespace std;

typedef unordered_map<string, Dependant*> DependantMap;

class DependantManager {
public:
	DependantManager();
	void addXlibComponentFactory(XlibComponentFactoryPtr xmgr);
	void load(json& cfg);
	/**
	 * @brief initial
	 * 
	 */
	void init();
	/**
	 * @brief get dependant map
	 * 
	 * @return DependantMap& 
	 */
	DependantMap& dependantMap() { return _depMap; }
	/**
	 * @brief Get the Dependant object
	 * 
	 * @param name 
	 * @return Dependant* 
	 */
	Dependant* getDependant(const string& name);
	/**
	 * @brief add dependant item
	 * 
	 * @param name 
	 * @param dep 
	 */
	void addDependant(const string& name, Dependant* dep);

public:
	static DependantManager* g_instance;
	static DependantManager* instance();

private:
	map<string,XlibComponentFactoryPtr> _xmgrs;
	DependantMap _depMap;
};
