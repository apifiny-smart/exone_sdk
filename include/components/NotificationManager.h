#pragma once
#include "common/common.h"

enum NotificationTypes {
	NT_UNKNOWN,
	NT_TIMER,
	NT_BOOK,
	NT_TRADE,
	NT_ORDER
};

class NotificationManager {
public:
	NotificationManager() {};
	/**
	 * @brief initial
	 * 
	 */
	void init() {}
	/**
	 * @brief Set the Notice Id object
	 * 
	 * @param nid 
	 */
	void setNoticeId(int nid) { _noticeId = nid; }
	/**
	 * @brief Get the Notice Id object
	 * 
	 * @return int 
	 */
	int getNoticeId() { return _noticeId; }
	/**
	 * @brief Set the Notification Type object
	 * 
	 * @param t 
	 */
	void setNotificationType(NotificationTypes t) { _type = t; }
	/**
	 * @brief Get the Notification Type object
	 * 
	 * @return NotificationTypes 
	 */
	NotificationTypes getNotificationType() { return _type; }
public:
	static NotificationManager* g_instance;
	static NotificationManager* instance();

private:
	NotificationTypes _type = NT_UNKNOWN;
	int _noticeId = 0;
};
