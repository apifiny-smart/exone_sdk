#pragma once
#include <map>
#include "base/Strategy.h"
#include "base/utils.h"
#include "base/XlibComponentFactory.h"

using namespace std;

typedef vector<Strategy*> StrategyVec;
typedef map<string, Strategy*> StrategyMap;
typedef map<string, XlibComponentFactoryPtr> XlibMap;

class StrategyManager {
public:
	StrategyManager();
	/**
	 * @brief add XlibComponentFactory
	 * 
	 * @param xmgr 
	 */
	void addXlibComponentFactory(XlibComponentFactoryPtr xmgr);
	/**
	 * @brief load config json
	 * 
	 * @param cfg 
	 */
	void load(json& cfg);
	/**
	 * @brief initial
	 * 
	 */
	void init();
	void close();
	StrategyVec& strategies();
	StrategyMap& strategyMap() { return _strategyMap; }
	/**
	 * @brief Get the Strategy object
	 * 
	 * @param name 
	 * @return Strategy* 
	 */
	Strategy* getStrategy(const string& name);
	/**
	 * @brief add strategy
	 * 
	 * @param name 
	 * @param strategy 
	 */
	void addStrategy(const string& name, Strategy* strategy);
	/**
	 * @brief get xlib map
	 * 
	 * @return XlibMap& 
	 */
	XlibMap& xlibMap() {return _xmgrs;}

	void notify(long id);

public:
	static StrategyManager* g_instance;
	static StrategyManager* instance();

private:
	XlibMap _xmgrs;
	StrategyMap _strategyMap;
	StrategyVec _strategies;
	int _numStrats = 0;
};
