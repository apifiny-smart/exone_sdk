#pragma once
#include "base/Printer.h"
#include "base/utils.h"
#include "base/XlibComponentFactory.h"

using namespace std;

typedef unordered_map<string, Printer*> PrinterMap;
typedef vector<Printer*> PrinterVec;

class PrinterManager {
public:
	PrinterManager();
	void addXlibComponentFactory(XlibComponentFactoryPtr xmgr);
	void load(json& cfg, int tradeDate);
	void init();
	void close();
	PrinterMap& printerMap() { return _printerMap; }
	Printer* getPrinter(const string& name);
	void addPrinter(const string& name, Printer* printer);

	void notify(long id);

public:
	static PrinterManager* g_instance;
	static PrinterManager* instance();

private:
	PrinterMap _printerMap;
	PrinterVec _printers;
	int _numPrinters = 0;
	map<string,XlibComponentFactoryPtr> _xmgrs;
};
