#pragma once
#include "common/common.h"
#include "common/minipool.hpp"
#include "base/DataTypes.h"

using namespace std;

class MempoolManager {
public:
	MempoolManager();

	static Message* allocMessage();
	static void free(Message* o);

	static PBCcRaw* allocPBCcRaw();
	static void free(PBCcRaw* o);

	static PBCCJsonRaw* allocPBCCJsonRaw();
	static void free(PBCCJsonRaw* o);

	static PBQuote3* allocPBQuote3();
	static void free(PBQuote3* o);

	static PBQuote2* allocPBQuote2();
	static void free(PBQuote2* o);

	static PBQuote* allocPBQuote();
	static void free(PBQuote* o);

	static PBTrade2* allocPBTrade2();
	static void free(PBTrade2* o);

	static PBTrade* allocPBTrade();
	static void free(PBTrade* o);

	static PBOrderReq* allocPBOrderReq();
	static void free(PBOrderReq* o);

	static PBOrderCxlReq* allocPBOrderCxlReq();
	static void free(PBOrderCxlReq* o);

public:
	static MempoolManager* g_instance;
	static MempoolManager* instance();

private:
	minipool<Message> * _msgPool;
	minipool<PBCcRaw> * _PBCcRawPool;
	minipool<PBCCJsonRaw> * _PBCCJsonRawPool;
	minipool<PBQuote> * _PBQuotePool;
	minipool<PBQuote2> * _PBQuote2Pool;
	minipool<PBQuote3> * _PBQuote3Pool;
	minipool<PBTrade> * _PBTradePool;
	minipool<PBTrade2> * _PBTrade2Pool;
	minipool<PBOrderReq> * _PBOrderReqPool;
	minipool<PBOrderCxlReq> * _PBOrderCxlReqPool;
};
