#pragma once

#include "base/TimerListener.h"
#include "common/common.h"
#include <map>

using namespace std;

class TimerHandler {
public:
	TimerHandler(long msecs);
	TimerHandler(const nanoseconds& ts, bool isOneShot=true);
	void addListener(TimerListener *l);
	/**
	 * @brief process ontimer
	 * 
	 */
	void fireOnTimer();
	/**
	 * @brief is oneshot?
	 * 
	 * @return true 
	 * @return false 
	 */
	bool isOneShot() { return _isOneShot; }
	/**
	 * @brief get the Timeout object
	 * 
	 * @return nanoseconds& 
	 */
	nanoseconds& timeout() { return _next_timeout; }
	/**
	 * @brief Set the Timeout object
	 * 
	 * @param ts 
	 */
	void setTimeout(nanoseconds& ts) { _next_timeout = ts; }
	/**
	 * @brief get the duration
	 * 
	 * @return nanoseconds& 
	 */
	nanoseconds& dur() { return _dur;}
	//long duration() { return _msecs;}

private:
	bool _isOneShot = false;
	nanoseconds _dur;
	long _msecs;
	nanoseconds _next_timeout;
	vector<TimerListener *> _listeners;
};
typedef unordered_map<long, TimerHandler*> TimerMap;
typedef std::multimap<long, TimerHandler*> OneShotTimerMap;

class TimerManager {
public:
	TimerManager();
	/**
	 * @brief initial
	 * 
	 */
	void init() {}
	/**
	 * @brief Get the Timer Handler object
	 * 
	 * @param msecs 
	 * @return TimerHandler* 
	 */
	TimerHandler* getTimerHandler(long msecs);
	/**
	 * @brief Get the One Shot Timer Handler object
	 * 
	 * @param ts 
	 * @return TimerHandler* 
	 */
	TimerHandler* getOneShotTimerHandler(const nanoseconds& ts);
	/**
	 * @brief Set the Current Time object
	 * 
	 * @param t 
	 */
	void setCurrentTime(const nanoseconds& t);
	/**
	 * @brief get the Current Time object
	 * 
	 * @return const nanoseconds& 
	 */
	const nanoseconds& currentTime() { return _now; }
	std::string timeStr();

private:
	void processTimers(const nanoseconds& nextEventTime);

public:
	static TimerManager* g_instance;
	static TimerManager* instance();

private:
	OneShotTimerMap _oneShotTimerMap;
	TimerMap _timerMap;
	nanoseconds _now;
};
