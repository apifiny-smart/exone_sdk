#pragma once
#include "base/RiskFormula.h"
#include "common/common.h"

using namespace std;

typedef unordered_map<string, RiskFormula*> RiskFormulaMap;

class RiskFormulaManager {
public:
	RiskFormulaManager();
	void load(json& cfg);
	void init();
	RiskFormulaMap& formulaMap() { return _formulaMap; }
	RiskFormula* getRiskFormula(const string& name);
	void addRiskFormula(const string& name, RiskFormula* formula);

public:
	static RiskFormulaManager* g_instance;
	static RiskFormulaManager* instance();

private:
	RiskFormulaMap _formulaMap;
};
