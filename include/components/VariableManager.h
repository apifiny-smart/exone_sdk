#pragma once
#include <vector>
#include "base/utils.h"
#include "base/Variable.h"
#include "base/XlibComponentFactory.h"

using namespace std;

typedef unordered_map<string, Variable*> VariableMap;

class VariableManager {
public:
	VariableManager();	
	void addXlibComponentFactory(XlibComponentFactoryPtr xmgr);
	void load(json& cfg);
	void init();
	VariableMap& variableMap() { return _varMap; }
	Variable* getVariable(const string& name);
	void addVariable(const string& name, Variable* var);

public:
	static VariableManager* g_instance;
	static VariableManager* instance();

private:
	map<string,XlibComponentFactoryPtr> _xmgrs;
	VariableMap _varMap;
	typedef unordered_map<string, string> AliasMap;
	AliasMap _aliasMap;
};
