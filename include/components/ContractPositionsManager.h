#pragma once
#include "common/common.h"

struct ContractPositionsItem {
	double long_pos = 0.0;
	double short_pos = 0.0;
	double net_pos = 0.0;
};
typedef unordered_map<string, ContractPositionsItem* > ContractPositionsMap;

class ContractPositionsManager {
public:
	/**
	 * @brief get contract position map
	 * 
	 * @return ContractPositionsMap& 
	 */
	ContractPositionsMap& contractPositionsMap() { return _contractPositionsMap; }
	/**
	 * @brief Get the contract position Item object
	 * 
	 * @param itemName 
	 * @return ContractPositionsItem* 
	 */
	ContractPositionsItem* getItem(const string& itemName);

public:
	static ContractPositionsManager* g_instance;
	static ContractPositionsManager* instance();

private:
	ContractPositionsMap _contractPositionsMap;
};
