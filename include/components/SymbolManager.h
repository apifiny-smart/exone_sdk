#pragma once
#include "base/SymbolHandler.h"
#include "base/TimerListener.h"

typedef unordered_map<long, SymbolHandler*> CidSymbolMap;
typedef unordered_map<string, SymbolHandler*> NameSymbolMap;

class SymbolManager: public TimerListener {
public:
	SymbolManager();
	/**
	 * @brief initial
	 * 
	 */
	void init();
	/**
	 * @brief get symbol map
	 * 
	 * @return CidSymbolMap& 
	 */
	CidSymbolMap& symbolMap() { return _cidSymMap; }
	/**
	 * @brief Get the Symbol Handler object
	 * 
	 * @param cid 
	 * @return SymbolHandler* 
	 */
	SymbolHandler* getSymbolHandler(long cid) { return _cidSymMap[cid]; }
	/**
	 * @brief find Symbol Handler
	 * 
	 * @param ticker 
	 * @param exch 
	 * @return SymbolHandler* 
	 */
	SymbolHandler* findSymbolHandler(const string& ticker, const string& exch);
	/**
	 * @brief Get the Symbol Handler object
	 * 
	 * @param ticker 
	 * @param exch 
	 * @return SymbolHandler* 
	 */
	SymbolHandler* getSymbolHandler(const string& ticker, const string& exch);
	/**
	 * @brief add symbol
	 * 
	 * @param ticker 
	 * @param exch 
	 * @param cid 
	 * @return SymbolHandler* 
	 */
	SymbolHandler* addSymbol(const string& ticker, const string& exch, long cid=0);

	void onTimer(TimerHandler* th, long msecs) override;

public:
	static SymbolManager* g_instance;
	static SymbolManager* instance();

private:
	NameSymbolMap _nameSymMap;
	CidSymbolMap _cidSymMap;
	long _next_cid = 100000; //custom cid range: 100000 -
};
