#pragma once
#include "base/Recorder.h"
#include "common/common.h"

using namespace std;

typedef unordered_map<string, Recorder*> RecorderMap;

class RecorderManager {
public:
	RecorderManager();
	void load(json& cfg, int tradeDate);
	void init();
	RecorderMap& recorderMap() { return _recorderMap; }
	Recorder* getRecorder(const string& name);
	void addRecorder(const string& name, Recorder* recorder);

public:
	static RecorderManager* g_instance;
	static RecorderManager* instance();

private:
	RecorderMap _recorderMap;
};
