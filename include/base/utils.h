#pragma once
#include "common/common.h"
/**
 * @brief apply the log devider
 * 
 * @param v 
 * @param divider 
 * @param exp 
 * @return double 
 */
double apply_log_divider(double v, double divider, double exp);
/**
 * @brief calculate the decay through halflife
 * 
 * @param halflife 
 * @return double 
 */
double calc_decay(double halflife);
/**
 * @brief calculate the  halflife through decay
 * 
 * @param decay 
 * @return double 
 */
double calc_halflife(double decay);

class EMS {
public:
	EMS() : _value(0.0) {}
	void reset(double v=0.0);
	void update(double v, double decay);
	double value() { return _value; }
private:
	double _value;
};

class EMA {
public:
	EMA() : _value(0.0) {}
	void reset(double v);
	void update(double v, double decay);
	double value() { return _value; }
private:
	double _value;
};

class EMA2 {
public:
	EMA2() : _value(0.0) {}
	void reset(double v);
	void decay(double decay, int sampleCnt);
	void update(double v);
	double value() { return _value; }
private:
	double _value = 0.0;
	bool _updated = true;
	double _lastX = 0.0;
	double _weights = 0.0;
	double _weightedSum = 0.0;
};

class EMA3 {
public:
	EMA3() : _value(0.0) {}
	EMA3(double v)  { reset(v); }
	void reset(double v);
	void decay(double decay, int sampleCnt);
	void update(double v);
	double value() { return _value; }
	bool is_ready() { return _weights > EPS; }
private:
	double _value = 0.0;
	double _lastX = 0.0;
	double _weights = 0.0;
};

/*
class EMA3 {
public:
	EMA3() : _value(0.0) {}
	EMA3(double v)  { reset(v); }
	void reset(double v);
	void decay(double decay, int sampleCnt);
	void update(double v);
	double value() { return _value; }
	bool is_ready() { return _weights > EPS; }
private:
	double _value = 0.0;
	double _lastX = 0.0;
	double _weights = 0.0;
	double _weightedSum = 0.0;
};
*/

class EMA3b {
public:
	EMA3b(int minSampleCnt=1) : _minSampleCnt(minSampleCnt) {}
	void reset(double v);
	void decay(double decay, int sampleCnt);
	void update(double v);
	double value() { return _value; }
	bool is_ready() { return _ready; }
private:
	double _value = 0.0;
	double _lastX = 0.0;
	double _weights = 0.0;
	int _minSampleCnt = 1;
	double _initSum = 0.0;
	bool _ready = false;
};

class EMA4 {
public:
	EMA4(int minSampleCnt=1) : _value(0.0), _minSampleCnt(minSampleCnt) {}
	void reset(double v);
	void decay(double decay, int sampleCnt);
	void update(double v);
	double value() { return _value; }
	bool is_ready() { return _ready; }
private:
	bool _ready = false;
	int _sampleCnt = 0;
	int _minSampleCnt = 1;
	double _value = 0.0;
	double _lastV = 0.0;
	double _lastMax = -1e10;
	double _lastMin = 1e10;
	double _weights = 0.0;
	double _weightedSum = 0.0;
};

//Exponential Moving Correlation
class EMCorr {
public:
	EMCorr() {}
	void reset();
	void decay(double decay, int sampleCnt);
	void update(double x, double y);
	double value() { return _value; }
	bool is_ready() { return _sampleCnt > 10; }
	int sampleCnt() { return _sampleCnt; }
private:
	double _value = 0.0;
	int _sampleCnt = 0;
	double _meanX = 0.0;
	double _meanY = 0.0;
	double _meanXY = 0.0;
	double _varX = 0.0;
	double _varY = 0.0;
	double _covXY = 0.0;
	double _x;
	double _y;
	bool _hasData = false;
};

//Exponential Moving Regresssion
class EMR {
public:
	EMR() {}
	void reset();
	void decay(double decay, int sampleCnt);
	void update(double x, double y);
	double value(double x) { return _a * x + _b; }
	double slope() { return _a; }
	double trend() { return misceq(_meanY, 0.0) ? 0.0 : _a / _meanY; }
	double corr() { return (_varX < EPS || _varY < EPS) ? 0.0 : _covXY / (sqrt(_varX) * sqrt(_varY)); }
	bool is_ready() { return _sampleCnt > 10; }
	int sampleCnt() { return _sampleCnt; }
private:
	double _a = 0.0;
	double _b = 0.0;
	int _sampleCnt = 0;
	double _meanX = 0.0;
	double _meanY = 0.0;
	double _meanXY = 0.0;
	double _varX = 0.0;
	double _varY = 0.0;
	double _covXY = 0.0;
	double _x;
	double _y;
	bool _hasData = false;
};
