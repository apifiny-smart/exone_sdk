#pragma once
#include "common/common.h"
#include "base/Consts.h"
#include "base/Order.h"
/**
 * @brief The base class for the management class of order,you can view the status of the current order in the market
 * 
 */
class IOrderManager {
public:
	virtual ~IOrderManager() {}

	//return the size before order, limited to the same price level only
	//Special return values: -1, order has been swept by trades
	//                       -2, order has been crossed
	//                       -3, order is not in valid state for such query
	/**
	 * @brief return the size before order, limited to the same price level only
	 * 
	 * @param order 
	 * @return double. -1, order has been swept by trades ;-2, order has been crossed ;-3, order is not in valid state for such query
	 */
	virtual double getSizeBeforeAtSameLevel(LOrder* order) = 0;
};
