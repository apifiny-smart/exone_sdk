#pragma once
#include <string>
#include <vector>

#include "base/Notifiable.h"

using namespace std;

class ParamRecord {
public:
	ParamRecord(int numDeps, int numVars) {
		_deps.resize(numDeps);
		_vars.resize(numVars);
		_numDeps = numDeps;
	}

	void setTimestamp(nanoseconds& ts) { _ts = ts; }
	nanoseconds timestamp() { return _ts;}

	void addDepValue(int i, double v) {
		_deps[i] = v;
		_numAddedDeps++;
	}
	const vector<double>& depValues() { return _deps; }
	vector<double>& varValues() { return _vars; }

	bool ready() { return _numAddedDeps == _numDeps; }

private:
	nanoseconds _ts;
	/**
	 * @brief ??
	 * 
	 */
	vector<double> _deps;
	/**
	 * @brief ??
	 * 
	 */
	vector<double> _vars;
	int _numDeps;
	int _numAddedDeps = 0;
};
/**
 * @brief ??
 * 
 */
typedef list<ParamRecord*> ParamData;
