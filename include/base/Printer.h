#pragma once
#include "base/SymbolNotifiableTree.h"
#include "base/JsonConfigurable.h"
#include "base/SymbolListener.h"
#include "base/SamplerListener.h"
#include "base/TimerListener.h"

class Printer: 	public virtual SymbolNotifiableTree,
				public SymbolListener,
				public SamplerListener,
				public TimerListener,
				public JsonConfigurable {
public:
	using JsonConfigurable::JsonConfigurable;
	void setActive(bool active) { _active = active; }

protected:
	bool _active = true;
};

