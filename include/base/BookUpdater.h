#pragma once
#include "common/common.h"

class DepthUpdate;
/**
 * @brief Used to update book data
 * 
 */
class BookUpdater {
public:
	enum UpdateResults {
		NOP,
		L1_CHANGE,
		L2_CHANGE
	};
	
	virtual ~BookUpdater() {}
	
	virtual int updateBook(DepthUpdate* update) = 0;
};
