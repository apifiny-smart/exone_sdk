#pragma once
#include "common/common.h"
#include "base/IOrderBookManager.h"

typedef map<int64_t, Level> LevelMap;
typedef LevelMap::iterator LevelMapIt;

typedef vector<Level> LevelVec;
typedef LevelVec::iterator LevelVecIt;

// MapLevelBook implementation
class MapLevelAccessor: public LevelAccessor {
public:
	MapLevelAccessor(LevelMap& map) : _levelMap(map) {}
	Level* begin() {
		_it = _levelMap.begin();
		return (_it != _levelMap.end()) ?  &_it->second : NULL;
	}
	Level* next() {
		++_it;
		return (_it != _levelMap.end()) ?  &_it->second : NULL;
	}
private:
	LevelMapIt _it;
	LevelMap& _levelMap;
};

class MapLevelBook {
public:
	static const int64_t KEY_LIMIT = 1000000000000L;
	MapLevelBook(TradeDirs side) : _levelAccessor(_levelMap), _side(side), _ascending(side==SELL) {}
	LevelAccessor& getLevelAccessor() { return _levelAccessor; }
	LevelMap& getLevelMap() { return _levelMap; }
	Level* getTopLevel() { return _levelMap.empty() ? NULL : &_levelMap.begin()->second; }
	int64_t getTopKey() { return _levelMap.empty() ? KEY_LIMIT : _levelMap.begin()->first; }

	int64_t getKey(double price) {
		int64_t key = price * 1e8 + 0.1; //EPS;
		return _ascending ? key : -key;
	}
protected:
	TradeDirs _side;
	bool _ascending;
	LevelMap _levelMap;
	MapLevelAccessor _levelAccessor;
};

class MapBookManager: public IOrderBookManager {
public:
	MapBookManager() : _bidBook(BUY), _askBook(SELL)  {}
	LevelAccessor& getBidLevelAccessor() { return _bidBook.getLevelAccessor(); }
	LevelAccessor& getAskLevelAccessor() { return _askBook.getLevelAccessor(); }

	[[deprecated("use LevelAccessor instead")]]
	bool getNthLevelInfo(int cid, size_t nth, TradeDirs side, double &px, double& levelSize, int& nOrders, bool isPrevBook=false) {
		throw std::runtime_error("getNthLevelInfo deprecated, use LevelAccessor instead");
		return true;
	}

protected:
	MapLevelBook _bidBook;
	MapLevelBook _askBook;
};


// VecLevelBook implementation
class VecLevelAccessor: public LevelAccessor {
public:
	VecLevelAccessor(LevelVec& levels) : _levels(levels) {}
	Level* begin() {
		_it = _levels.begin();
		return (_it != _levels.end()) ?  &(*_it) : NULL;
	}
	Level* next() {
		++_it;
		return (_it != _levels.end()) ?  &(*_it) : NULL;
	}
private:
	LevelVecIt _it;
	LevelVec& _levels;
};

class VecLevelBook {
public:
	VecLevelBook() : _levelAccessor(_levels) {}
	LevelAccessor& getLevelAccessor() { return _levelAccessor; }
	LevelVec& getLevels() { return _levels; }
	Level* getTopLevel() { return _levels.empty() ? NULL : &_levels.front(); }
protected:
	LevelVec _levels;
	VecLevelAccessor _levelAccessor;
};

class VecBookManager: public IOrderBookManager {
public:
	VecBookManager() {}
	LevelAccessor& getBidLevelAccessor() { return _bidBook.getLevelAccessor(); }
	LevelAccessor& getAskLevelAccessor() { return _askBook.getLevelAccessor(); }

	[[deprecated("use LevelAccessor instead")]]
	bool getNthLevelInfo(int cid, size_t nth, TradeDirs side, double &px, double& levelSize, int& nOrders, bool isPrevBook=false) {
		throw std::runtime_error("getNthLevelInfo deprecated, use LevelAccessor instead");
		return true;
	}

protected:
	VecLevelBook _bidBook;
	VecLevelBook _askBook;
};

