#pragma once
#include "base/Notifiable.h"
#include "base/JsonConfigurable.h"
#include "base/SymbolListener.h"
#include "base/DataTypes.h"
/**
 * @brief Input external data such as configuration data in real market and market data in backtesting
 * 
 */
class Player: public JsonConfigurable {
public:
	using JsonConfigurable::JsonConfigurable;
	/**
	 * @brief ??
	 * 
	 * @param nextTS 
	 * @return Message* 
	 */
	virtual Message* readNextMessage(std::chrono::nanoseconds& nextTS) = 0;

	std::chrono::nanoseconds _latency;
};

