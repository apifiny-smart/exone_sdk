#pragma once

enum AssetClass {
	CC,
	CC_USD_FUTURES,
	STOCK,
	FUTURES,
	OPTIONS,
	NUM_ASSET_CLASS
};
const char* const AssetClassNames[] = {
	"CC",
	"CC_USD_FUTURES",
	"STOCK",
	"FUTURES",
	"OPTIONS",
	"NUM_ASSET_CLASS"
};

enum TradeDirs {
	SELL,
	BUY,
	NUM_TRADE_DIRS
};
const char* const TradeDirNames[] = {
	"SELL",
	"BUY",
	"NUM_TRADE_DIRS"
};

enum OrderIntention {
	OI_UNKNOWN,
	OI_AGGRESSIVE,
	OI_PASSIVE,
	OI_HEDGE,
	OI_CLOSE
};

const char* const OrderIntentionNames[] = {
	"UNKNOWN",
	"AGGRESSIVE",
	"PASSIVE",
	"HEDGE",
	"CLOSE"
};

enum class Tif
  : char {
    TIF_UNKNOWN = 'U',
    TIF_IOC = 'I',
    TIF_DAY = 'D',
    TIF_ALL='A' //special type for all TIFs
};

enum OrderType {
	UNKNOWN_ORDERTYPE = -1,
	MARKET,
	LIMIT,
	MAX_ORDERTYPE,
	ALL //special type for all orders
};

enum class Liquidity
  : char {
    LI_UNKNOWN = 0,
    LI_ADD = 1,
	LI_REMOVE = 2
};

const char* const LiquidityNames[] = {
	"UNKNOWN",
	"ADD",
	"REMOVE"
};
