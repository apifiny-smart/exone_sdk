#pragma once
#include "common/common.h"
#include "common/concurrentqueue.h"
#include "common/blockingconcurrentqueue.h"

using namespace moodycamel;
/**
 * @brief This basic class contains the basic information of request an order
 * 
 */
class OrderReq {
public:
	enum ReqTypes {
		CREATE,
		CANCEL
	};
	OrderReq(ReqTypes t) : reqType(t) {}
	virtual ~OrderReq() {}
	ReqTypes reqType;
};
/**
 * @brief This class contains the basic information of request an create order,such as ticker,side,qty,px ...
 * 
 */
class OrderCreateReq: public OrderReq {
public:
	OrderCreateReq() : OrderReq(CREATE) {}
	string local_name;
	string ticker;
	string exchange;
	TradeDirs side;
	Tif tif;
	bool use_margin;
	string margin_source;
	double qty;
	double px;
	long orderId;
	/**
	 * @brief Maximum leverage for an order,default 1
	 * 
	 */
	long lever = 1;
	/**
	 * @brief ??
	 * 
	 */
	string octype; //for futures only
};
/**
 * @brief This class contains the basic information of request a cancel order
 * 
 */
class OrderCancelReq: public OrderReq {
public:
	OrderCancelReq() : OrderReq(CANCEL) {}
	string local_name;
	bool use_margin;
	string margin_source;
	long orderId;
	long remoteOrderId;
};

/**
 * @brief This basic class contains the basic response of order
 * 
 */
class OrderResp {
public:
	enum RespTypes {
		CREATE,
		CANCEL,
		STATUS
	};
	OrderResp(RespTypes t) : respType(t) {}
	virtual ~OrderResp() {}
	RespTypes respType;
};
/**
 * @brief This class contains the basic response of create an order,such as success,result...
 * 
 */
class OrderCreateResp: public OrderResp {
public:
	OrderCreateResp() : OrderResp(CREATE) {}
	bool success = false;
	long orderId;
	long remoteOrderId;
	std::string remoteOrderId_S;
	json result;
};
/**
 * @brief This class contains the basic response of cancel an order,such as success...
 * 
 */
class OrderCancelResp: public OrderResp {
public:
	OrderCancelResp() : OrderResp(CANCEL) {}
	bool success = false;
	long orderId;
};

class IBOrderStatusResp: public OrderResp {
public:
	IBOrderStatusResp() : OrderResp(STATUS) {}
	long remoteOrderId;
	string status;
	double filledQty;
	double avgFillPrice;
};

class OrderRespHub {
public:
	virtual ~OrderRespHub() {}
	virtual void enqueueOrderResp(OrderResp* msg) = 0;
};
