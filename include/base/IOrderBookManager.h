#pragma once
#include "common/common.h"
#include "base/Consts.h"

class Level {
public:
	Level() {}
	Level(double p, double q, int o) : price(p), qty(q), nords(o) {}
public:
	double price = 0.0;
	double qty = 0.0;
	int nords = 0;
};

class LevelAccessor {
public:
	virtual ~LevelAccessor() {}
	virtual Level* begin() = 0;
	virtual Level* next() = 0;
};

class DummyLevelAccessor: public LevelAccessor {
public:
	Level* begin() { return NULL; }
	Level* next() { return NULL; }
};

class IOrderBookManager {
public:
	virtual ~IOrderBookManager() {}

	virtual bool getNthLevelInfo(int cid, size_t nth, TradeDirs side,
	      double &px, double& levelSize, int& nOrders, bool isPrevBook=false) = 0;

	virtual LevelAccessor& getBidLevelAccessor() { return dummyLevelAccessor; }
	virtual LevelAccessor& getAskLevelAccessor() { return dummyLevelAccessor; }
	LevelAccessor& getLevelAccessor(TradeDirs side) { return (side==BUY) ? getBidLevelAccessor() : getAskLevelAccessor(); }

	DummyLevelAccessor dummyLevelAccessor;
};
