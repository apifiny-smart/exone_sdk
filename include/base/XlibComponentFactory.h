#pragma once
#include <vector>
#include "utils.h"
#include "Sampler.h"
#include "PricingModel.h"
#include "Variable.h"
#include "Strategy.h"
#include "Dependant.h"
#include "Model.h"
#include "Printer.h"
#include <boost/config.hpp>
#include <boost/dll/import.hpp> 

using namespace std;

//reference: https://www.boost.org/doc/libs/1_76_0/doc/html/boost_dll/tutorial.html
/**
 * @brief The factory class of xlib, which is used to load the custom recognition of various components into the program
 * 
 */
class BOOST_SYMBOL_VISIBLE XlibComponentFactory {
public:
   	virtual ~XlibComponentFactory() {}
    virtual string getNameSpaceName() { return ""; }
	virtual Sampler* getSampler(const string& type, const string& name, const json& attr) { return nullptr; }
	virtual PricingModel* getPricingModel(const string& type, const string& name, const json& attr) { return nullptr; }
    virtual Variable* getVariable(const string& type, const string& name, const json& attr) { return nullptr; }
    /**
     * @brief Get the Strategy object
     * 
     * @param type custom strategy class
     * @param name custom strategy class name
     * @param attr Configuration properties of custom policy classes
     * @return Strategy* 
     */
    virtual Strategy* getStrategy(const string& type, const string& name, const json& attr) { return nullptr; }
    virtual Dependant* getDependant(const string& type, const string& name, const json& attr) { return nullptr; }
    virtual Model* getModel(const string& type, const string& name, const json& attr) { return nullptr; }
    virtual Printer* getPrinter(const string& type, const string& name, const json& attr,int tradeDate) { return nullptr; }

};
typedef boost::shared_ptr<XlibComponentFactory> XlibComponentFactoryPtr;