#pragma once

#include "common/common.h"

// Both long and long long are 64-bits on linux 64-bit os.

enum PBTypes{
	QUOTE,  		//market data - quotes
	TRADE,			//market data - trades
	ORDER_REQ,		//sim - order request
	ORDER_CXL_REQ,	//sim - cancel request
	ORDER_MATCH,	//sim - order match at exchange
	//ORDER_EXEC		//sim - execution
	QUOTE2,  		//market data - full top quotes
	TRADE2,			//market data - full trades
	QUOTE3,  		//market data - level quotes
	CCRAW,			//market data - cc raw data
	CCJSONRAW 	//market data - ccjson raw data
};

struct PBType {
	virtual ~PBType() {}
};

struct PBCcRaw: PBType {
	uint64_t time = 0;
	char action;
	string data;
};

struct PBCCJsonRaw: PBType {
	uint64_t time = 0;
	char action;
	string data;
};

struct PBTrade: PBType {
	uint64_t time = 0;
	double trade_px = 0;
	double trade_qty = 0;
};

struct PBQuote: PBType {
	uint64_t time = 0;
	double bid_px = 0;
	double bid_qty = 0;
	double ask_px = 0;
	double ask_qty = 0;
};

struct PBTrade2: PBType {
	uint64_t time = 0;
	double trade_px = 0;
	double trade_qty = 0;
	uint8_t aggr_side = 0;
};

struct PBQuote2: PBType {
	uint64_t time = 0;
	uint8_t side = 0;
	double px = 0;
	double qty = 0;
	uint64_t ords = 0;
};

struct PBQuote3: PBType {
	struct BookLevel {
		BookLevel() {}
		BookLevel(double p, double q) : price(p), qty(q) {}
		double price = 0;
		double qty = 0;
	};
	uint64_t time = 0;
	vector<BookLevel> bidLevels;
	vector<BookLevel> askLevels;
};

struct PBOrderReq: PBType {
	uint32_t account = 0;
	uint8_t side = 0;
	double px = 0;
	double qty = 0;
	uint64_t order_id = 0;
};

struct PBOrderCxlReq: PBType {
	uint32_t account = 0;
	uint64_t order_id = 0;
};

/*
struct PBOrderExec: PBType {
	uint64_t time = 0;
	uint32_t account = 0;
	uint8_t side = 0;
	double px = 0;
	double qty = 0;
	int8_t liq = 0;
	uint64_t order_id = 0;
};
*/

template<typename T, typename std::enable_if<std::is_base_of<PBType, T>::value>::type* = nullptr>
std::ostream& operator<<(std::ostream& out, const T& h)
{
     out.write(reinterpret_cast<const char*>(&h), sizeof(T));
     return out;
}

template<typename T, typename std::enable_if<std::is_base_of<PBType, T>::value>::type* = nullptr>
std::istream& operator>>(std::istream& in, T& h)
{
	T values; // use extra instance, for setting result transactionally
    in.read( reinterpret_cast<char*>(&values), sizeof(T) );
    if(in)
        h = std::move(values);
    return in;
}



struct PBTypeOld {
};

struct PBTradeOld: PBTypeOld {
	uint64_t time = 0;
	double trade_px = 0;
	uint64_t trade_qty = 0;
};

struct PBQuoteOld: PBTypeOld {
	uint64_t time = 0;
	double bid_px = 0;
	uint64_t bid_qty = 0;
	double ask_px = 0;
	uint64_t ask_qty = 0;
};

template<typename T, typename std::enable_if<std::is_base_of<PBTypeOld, T>::value>::type* = nullptr>
std::ostream& operator<<(std::ostream& out, const T& h)
{
     out.write(reinterpret_cast<const char*>(&h), sizeof(T));
     return out;
}

template<typename T, typename std::enable_if<std::is_base_of<PBTypeOld, T>::value>::type* = nullptr>
std::istream& operator>>(std::istream& in, T& h)
{
	T values; // use extra instance, for setting result transactionally
    in.read( reinterpret_cast<char*>(&values), sizeof(T) );
    if(in)
        h = std::move(values);
    return in;
}

struct Message{
	std::chrono::nanoseconds ts;
	long tickerId;
	PBTypes type;
	PBType* pb = nullptr;

	~Message();
};

struct MessagePtrComparer
{
  bool operator()(Message*& l, Message*& r)
  {
      return l->ts > r->ts;
  }
};

typedef priority_queue <Message*, std::deque<Message*>, MessagePtrComparer> MessageQueue;
