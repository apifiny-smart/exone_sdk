#pragma once
#include "base/JsonConfigurable.h"
#include "base/SamplerListener.h"
#include "base/utils.h"

using namespace std;
/**
 * @brief this class contains information about the lookback window and halflife for sampling data
 * 
 */
class Sampler: public virtual NotifiableLeaf, public JsonConfigurable {
public:
	using JsonConfigurable::JsonConfigurable;

	virtual void addListener(SamplerListener* l) {
		_listeners.push_back(l);
	}

	void onNotify() {}

protected:
	/**
	 * @brief Downsampling by the number of interval samples and decaying weights
	 * 
	 * @param cnt Number of interval samples
	 * @param decay decaying weights
	 */
	void fireOnSample(int cnt, double decay) {
		for(auto& l: _listeners) {
			l->onSample(this, cnt, decay);
		}
	}
	/**
	 * @brief Set the Halflife object
	 * 
	 * @param halflife 
	 */
	void setHalflife(double halflife) {
		_decay = calc_decay(halflife);
	}

protected:
	double _decay = 1.0;
	vector<SamplerListener *> _listeners;
};
