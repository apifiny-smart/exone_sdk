#pragma once
#include "base/JsonConfigurable.h"
#include "base/Notifiable.h"
#include "base/SamplerListener.h"
#include "base/SymbolListener.h"
#include "base/TimerListener.h"
#include "base/utils.h"
/**
 * @brief pricing_models indicates the type of prices used for the symbols used in the trading strategy
 * 
 */
class PricingModel: public virtual NotifiableLeaf, public SymbolListener, public SamplerListener, public TimerListener, public JsonConfigurable {
public:
	using JsonConfigurable::JsonConfigurable;
	/**
	 * @brief get previous close price
	 * 
	 * @return double 
	 */
	virtual double prevClosePrice() { return std::numeric_limits<double>::quiet_NaN(); }
	/**
	 * @brief get open price
	 * 
	 * @return double 
	 */
	virtual double open_price() { return _open_price; }
	/**
	 * @brief get price
	 * 
	 * @return double 
	 */
	virtual double price() { return _price; }
	/**
	 * @brief is pricingModel ready?
	 * 
	 * @return true 
	 * @return false 
	 */
	virtual bool is_ready() { return _ready; }
	/**
	 * @brief trigger callback on time
	 * 
	 * @param th 
	 * @param msecs 
	 */
	virtual void onTimer(TimerHandler* th, long msecs) {}
	/**
	 * @brief trigger callback on sample update
	 * 
	 * @param src 
	 * @param cnt 
	 * @param decay 
	 */
	virtual void onSample(Sampler* src, int cnt, double decay) {}
	/**
	 * @brief trigger callback on Quote update
	 * 
	 * @param cid 
	 * @param si 
	 */
	virtual void onQuote(long cid, const SymbolInfo& si) {}
	/**
	 * @brief trigger callback on tick update
	 * 
	 * @param cid 
	 * @param si 
	 */
	virtual void onTick(long cid, const SymbolInfo& si) {}
	/**
	 * @brief If the marketupdate is not received within 10 minutes, the status of the symbol will be set to unready
	 * 
	 * @param cid the unique identifier of the symbol
	 * @param si 
	 */
	virtual void onQuoteStale(long cid, const SymbolInfo& si) { _ready = false; setUpdated(); }

protected:
	double _open_price = 0.0;
	double _price = 0.0;
	bool _ready = false;
};
