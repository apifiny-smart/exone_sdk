#pragma once
#include "common/common.h"
#include "OrderMessage.h"
#include "base/MarketCommon.h"
/**
 * @brief base class for trader, containing common low-level methods
 * 
 */
class BaseTrader: public OrderRespHub {
public:
	
	~BaseTrader() {}
	/**
	 * @brief Timed events for timing triggers,notify NT_TIMER
	 * 
	 */
	virtual void onTimerEvent() = 0;
	/**
	 * @brief Callback fired when book changes,distrabute to processDepthMessage to process
	 * 
	 * @param msg book message
	 */
	virtual void onCCJsonBookChange(json& msg) = 0;
	/**
	 * @brief Callback fired by trades, distrabute to processTradeMessage to process
	 * 
	 * @param msg trades message
	 */
	virtual void onCCJsonTrade(json& msg) = 0;
	/**
	 * @brief Callback fired by userdata,distrabute to processOrderUpdate to process
	 * 
	 * @param msg 
	 */
	virtual void onUserData(json& msg) = 0;
	/**
	 * @brief Processing after receiving transaction information,update SymbolInfo...
	 * 
	 * @param cid the unique identifier of the symbol
	 * @param trade TradeUpdate contains symbol price,qty,trade_type
	 */
	virtual void processTradeMessage(int cid, TradeUpdate* trade) = 0;
	/**
	 * @brief Processing after receiving depth information,update SymbolInfo...
	 * 
	 * @param cid the unique identifier of the symbol
	 * @param bh DepthUpdate ,update levels
	 */
	virtual void processDepthMessage(int cid, DepthUpdate* bh) = 0;
};

