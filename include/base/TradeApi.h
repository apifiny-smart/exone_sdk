#pragma once
#include "common/common.h"
#include "base/PriceUtils.h"
#include "base/Order.h"
#include "base/SymbolInfo.h"
// #include "base/Account.h"
#include "base/IOrderManager.h"
/**
 * @brief Class for placing and canceling orders, interacting with redis
 * 
 */
class TradeApi: public IOrderManager {
public:
	virtual ~TradeApi() {}
	/**
	 * @brief initial
	 * 
	 */
	virtual void init() {};
	virtual LOrder* sendOrder(LOrder* order) = 0;
	virtual void cancelOrder(LOrder* order) = 0;
	virtual void publishCmdResp(json* data) {}
	/**
	 * @brief publish heartbeat
	 * 
	 * @param inst 
	 * @param state 
	 */
	virtual void publishHeartBeat(const string& inst, const string& state) {}
	/**
	 * @brief publish the model status
	 * 
	 * @param inst 
	 * @param stats 
	 */
	virtual void publishModelStats(const string& inst, json* stats) {}
};
