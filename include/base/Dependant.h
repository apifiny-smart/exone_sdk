#pragma once
#include "base/JsonConfigurable.h"
#include "base/Notifiable.h"
#include "base/ParamData.h"
#include "base/utils.h"

class Dependant: public NotifiableTree, public JsonConfigurable {
public:
	using JsonConfigurable::JsonConfigurable;
	virtual void addParamRecord(int depIndex, ParamRecord *prec) = 0;
};
