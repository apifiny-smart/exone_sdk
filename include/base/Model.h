#pragma once
#include "base/JsonConfigurable.h"
#include "base/Notifiable.h"
#include "base/Variable.h"
#include "base/SamplerListener.h"
/**
 * @brief allows the user to specify a model, combines the variable set to generate signal,such as linearModel
 * 
 */
class Model: public virtual NotifiableTree, public JsonConfigurable {
public:
	using JsonConfigurable::JsonConfigurable;
	/**
	 * @brief the signal of model output
	 * 
	 * @return double 
	 */
	virtual double signal() { return _signal; }
	virtual bool is_ready() { return _ready; }
	/**
	 * @brief Get the variable that the signal depends on
	 * 
	 * @param comps 
	 */
	virtual void getSignalComponents(SignalComponents& comps) = 0;

protected:
	double _signal = 0.0;
	bool _ready = false;
};
