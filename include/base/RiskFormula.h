#pragma once
#include "common/common.h"
#include "base/SymbolInfo.h"
#include "base/JsonConfigurable.h"

typedef unordered_map<long, double> SymbolMultipleMap;
/**
 * @brief risk_formulas defines the way to calculate account risk
 * 
 */
class RiskFormula: public JsonConfigurable {
public:
	using JsonConfigurable::JsonConfigurable;
	void init();
	/**
	 * @brief The risk multiplier for a symbol in the risk pairing
	 * 
	 * @param riskId Unique identifier for risk pairing
	 * @param cid the unique identifier of the symbol
	 * @return double 
	 */
	virtual double getRiskMultiple(long cid);
	/**
	 * @brief How many types of symbols are in a risk pairing
	 * 
	 * @param riskId Unique identifier for risk pairing
	 * @return int 
	 */
	int symbolCount() { return _symMap.size(); }

protected:
	SymbolMultipleMap _symMap;
};
