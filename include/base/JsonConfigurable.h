#pragma once
#include "common/common.h"

class JsonConfigurable{
public:
	JsonConfigurable() {}
	JsonConfigurable(const json& cfg) : _cfg(cfg) {}
	JsonConfigurable(const string& name, const json& cfg) : _name(name), _cfg(cfg) {}
	virtual ~JsonConfigurable() {}
	virtual void init() = 0;
	const string& name() { return _name; }

protected:
	bool try_init() {
		if(_initialized)
			return false;
		_initialized = true;
		return true;
	}

protected:
	string _name;
	json _cfg;
	bool _initialized = false;
};
