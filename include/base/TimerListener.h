#pragma once

class TimerHandler;

class TimerListener {
public:
	virtual ~TimerListener() {}
	/**
	 * @brief trigger callback on timer 
	 * 
	 * @param th 
	 * @param msecs 
	 */
	virtual void onTimer(TimerHandler* th, long msecs) = 0;
};

