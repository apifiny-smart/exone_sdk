#pragma once

#include "common/common.h"
#include "Consts.h"
#include "base/SymbolInfo.h"
#include "components/TimerManager.h"

class OrderSender;

enum CancelReasons {
	CR_UNKNOWN,
	CR_PAUSE_TRADING,
	CR_TIMEOUT,
	CR_NO_NEED,
	CR_LEVEL_MOVE,
	CR_SIGNAL_LOW,
	CR_SIGNAL_LOWER,
	CR_SIGNAL_HIGHER,
	CR_TOO_DEEP,
	CR_QTY_LOW,
	CR_INSIDE_BBO,
	CR_EXCEED_RISK,
	CR_NOT_SIGNAL_SIDE,
	CR_NO_TRADING_SIDE,
	CR_INVALID_SPREAD,
	CR_SIGNAL_NOT_READY
};
/**
 * @brief The order class, which contains the basic information of the order
 * 
 * sender:which instance send the order.
 * orderId:the unique local identifier of the order.
 * si:Contains basic information about the symbol.
 * use_margin:Whether to use margin mode .
 * margin_source:Is it a cross margin mode or an isolated margin mode.
 * signal:Signals on which to place an order.
 * intention:Active order or passive order.
 * 
 */
class LOrder {
public:
	// set when created
	OrderSender* sender;
	int noticeId = 0;

	uint64_t orderId;
	uint64_t remoteOrderId = 0;
	string remoteCobOrderId;
	uint64_t account;

	SymbolInfo* si;
	TradeDirs side;
	Tif tif = Tif::TIF_IOC;
	OrderType type = LIMIT;

	bool use_margin = false;

	string margin_source = "isolated";
	double px;
	double qty;

	double signal = 0.0;
	double cxl_signal = 0.0;
	double spread;

	OrderIntention intention = OI_UNKNOWN;

	// maintained by this order
	double filledQty = 0.0;
	double filledNotional = 0.0;
	double filledPx = 0.0; //for futures
	double remainingQty;
	nanoseconds sent_time = nanoseconds(0);
	nanoseconds ack_time = nanoseconds(0);
	nanoseconds rejected_time = nanoseconds(0);
	nanoseconds cancel_sent_time = nanoseconds(0);
	nanoseconds cancel_ack_time = nanoseconds(0);
	nanoseconds filled_time = nanoseconds(0);
	nanoseconds canceled_time = nanoseconds(0);
	nanoseconds closed_time = nanoseconds(0);
	bool closed = false;
	double qtyAhead = 0;
	
	// used and maintained by BaseStrategy
	bool needCancel = false;
	bool canceling = false;
	int cancel_cnt = 0;
	CancelReasons cancelReason = CR_UNKNOWN;
	double estQtyAhead = 0;
	
	// for measuring performance
	/**
	 * @brief The time when the order was triggered
	 * 
	 */
	nanoseconds trigger_event_time = nanoseconds(0);
	/**
	 * @brief received notice time,for measuring performance
	 * 
	 */
	nanoseconds notice_time = nanoseconds(0);

	long order_utime = 0;

public:
	
	void create();
	
	void ack();
	
	void reject();
	
	void cancelCreate();
	
	void cancelAck();
	
	void cancelReject();
	
	void fill(double fillPx, double fillQty, Liquidity liq);
	
	void cancel();
	
	void close();
	/**
	 * @brief ??
	 * 
	 * @param qh 
	 */
	void setQtyAhead(double qh) { qtyAhead = qh; }
};
typedef unordered_map<long, LOrder*> LOrderMap;
typedef unordered_map<std::string, LOrder*> SOrderMap;
typedef unordered_map<std::string, LOrder*> LCobOrderMap;

class MarketMessage;
typedef unordered_map<long, list<MarketMessage*>*> LOrderDataMap;
typedef unordered_map<std::string, list<MarketMessage*>*> SOrderDataMap;
/**
 * @brief This class is used to handle various state callbacks of order
 * 
 */
class OrderSender {
public:
	
	virtual ~OrderSender() {}
	
	virtual void onOrderCreated(LOrder* order) = 0;
	
	virtual void onOrderAcked(LOrder* order) = 0;
	
	virtual void onOrderRejected(LOrder* order) = 0;
	
	virtual void onOrderCancelCreated(LOrder* order) = 0;
	
	virtual void onOrderCancelAcked(LOrder* order) = 0;
	
	virtual void onOrderCancelRejected(LOrder* order) = 0;
	
	virtual void onOrderExec(TradeDirs side, double px, double qty, Liquidity liq, LOrder* order) = 0;
	
	virtual void onOrderCanceled(LOrder* order) = 0;
	
	virtual void onOrderClosed(LOrder* order) = 0;
};
