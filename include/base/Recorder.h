#pragma once
#include "base/Notifiable.h"
#include "base/JsonConfigurable.h"
#include "base/SymbolListener.h"

class Recorder: public SymbolListener, public virtual NotifiableTree, public JsonConfigurable {
public:
	using JsonConfigurable::JsonConfigurable;
};

