#pragma once
#include "common/common.h"
#include "base/Consts.h"
#include "base/IOrderBookManager.h"

class SymbolInfo;
class SymbolUtils
{
public:
	/**
	 * @brief Get the Trade Date object
	 * 
	 * @return long 
	 */
	static long getTradeDate();
	static bool isExchangeHandledByIB(const string &exch);
	// static long getPrevRollDate(const string& ticker, const string& exch);
	// static long getNextRollDate(const string& ticker, const string& exch);
	static string getProductName(const string &ticker, const string &exch);
	static string getFrontContractMonth(const string &ticker, const string &exch);
	static string getCurrencyFromLocalName(const string &localName, const string &exch);
	static string getTickerFromLocalName(const string &localName, const string &exch);
	static string getLocalSymbolName(const string &ticker, const string &exch);
	static string converToTradeVenue(const string &venue);
	static string converToCVenue(const string &venue);
	static string converToTradeSymbol(const string &symbol);
	static double getTickSize(const string &ticker, const string &exch);
	static double getLotSize(const string &ticker, const string &exch);
	static double getContractMultiplier(const string &ticker, const string &exch);
	static double getMinOrderSize(const string &ticker, const string &exch);
	static int getQtyPrecision(const string &ticker, const string &exch);
	static int getPricePrecision(const string &ticker, const string &exch);
	static int getLever(const string &ticker, const string &exch);
	static string getCoin(const string &ticker, const string &exch);
	static string getBaseCoin(const string &ticker, const string &exch);

	static string getMonthWord(std::string number);
	static string getMonthNum(std::string word);
	static string getVolatilitySymbolName(const string &ticker, const string &exch);
	static string getCurrencyFromTicker(const string &ticker, const string &exch);

	static double getVX0Weight();

	static string symbolFromChinaLocalSymbol(const string &local_symbol, const string &exch);
	static string localSymbolFromChinaSymbol(const string &symbol, const string &exch);

	static const string &getExchangeFullName(const string &cname);
	static const string &getExchangeCName(const string &fullname);

	static AssetClass getAssetClass(const string &ticker, const string &exch);

	static string getOptionsUnderlying(const string &ticker);
	static string getOptionsExpiry(const string &ticker);
	static double getOptionsStrike(const string &ticker);
	static bool getOptionsIsCall(const string &ticker);

protected:
	static unordered_map<string, string> g_chinaSymbol2LocalMap;
	static unordered_map<string, string> g_chinaLocal2SymbolMap;

	static void loadChinaSymbolMap();
	/**
	 * @brief find symbolinfo config json
	 * 
	 * @param ticker 
	 * @param exch 
	 * @param cfg 
	 * @return true 
	 * @return false 
	 */
	static bool findSymbolInfoCfg(const string& ticker, const string& exch, json& cfg);

	static bool findBaseSymbolInfoCfg(const string& ticker, const string& exch, json& cfg);
	/**
	 * @brief Get the Default Tick Size object
	 * 
	 * @param ticker 
	 * @param exch 
	 * @return double 
	 */
	static double getDefaultTickSize(const string& ticker, const string& exch);
	/**
	 * @brief Get the Default Lot Size object
	 * 
	 * @param ticker 
	 * @param exch 
	 * @return double 
	 */
	static double getDefaultLotSize(const string& ticker, const string& exch);
	/**
	 * @brief Get the Default Contract Multiplier object
	 * 
	 * @param ticker 
	 * @param exch 
	 * @return double 
	 */
	static double getDefaultContractMultiplier(const string& ticker, const string& exch);

	static string getDefaultCoin(const string &ticker, const string &exch);
	static string getDefaultBasecoin(const string &ticker, const string &exch);
};

class SymbolInfo
{
public:
	/**
	 * @brief is symbol ready for trade?
	 * 
	 * @return true 
	 * @return false 
	 */
	bool is_ready() const { return _ready; }
	/**
	 * @brief 
	 * 
	 * @return true 
	 * @return false 
	 */
	bool is_stale() const { return _stale; }
	double mid_px() const { return _mid_px; }
	double spread() const { return _spread; }
	double risk_px() const { return _risk_px; }
	// double open_vwap() const { return _open_vwap; }
	double open_mid_avg() const { return _open_mid_avg; }

	double mid_xbbo() const { return 0.5 * (bid_xbbo + ask_xbbo); }

	double ticks2Price(int ticks) const { return ticks * tick_size; }
	/**
	 * @brief Get the Ticks object
	 * 
	 * @param px 
	 * @return int 
	 */
	int getTicks(double px) const { return int((px + EPS) / tick_size); }
	/**
	 * @brief Get the Xbbo Price object
	 * 
	 * @param side 
	 * @return double 
	 */
	double getXbboPrice(TradeDirs side) const { return (side == BUY) ? bid_xbbo : ask_xbbo; }
	/**
	 * @brief Get the Tbbo Price object
	 * 
	 * @param side 
	 * @return double 
	 */
	double getTbboPrice(TradeDirs side) const { return (side == BUY) ? bid_tbbo : ask_tbbo; }
	/**
	 * @brief 
	 * 
	 * @param side 
	 * @return double 
	 */
	double getBBOPrice(TradeDirs side) const { return (side == BUY) ? bid_px : ask_px; }
	/**
	 * @brief 
	 * 
	 * @param side 
	 * @return double 
	 */
	double getBBOQty(TradeDirs side) const { return (side == BUY) ? bid_qty : ask_qty; }
	/**
	 * @brief Get the Best Price object
	 * 
	 * @param side 
	 * @return double 
	 */
	double getBestPrice(TradeDirs side) const { return (side == BUY) ? bid_px : ask_px; }
	/**
	 * @brief Get the Opposite Best Price object
	 * 
	 * @param side 
	 * @return double 
	 */
	double getOppositeBestPrice(TradeDirs side) const { return (side == SELL) ? bid_px : ask_px; }
	double improvedTicks(TradeDirs side, double tpx, double px) const
	{
		double ticks = (tpx - px) / tick_size;
		return (side == BUY) ? ticks : -ticks;
	}
	double getImprovedPrice(double improvedTicks, TradeDirs side) const
	{
		double improvedPx = improvedTicks * tick_size;
		return (side == BUY) ? bid_px + improvedPx : ask_px - improvedPx;
	}
	/**
	 * @brief Set the Prev Close Price object
	 * 
	 * @param px 
	 */
	void setPrevClosePrice(double px) { _prevClosePx = px; }
	/**
	 * @brief Get the prevous close Price 
	 * 
	 * @return double 
	 */
	double prevClosePrice() { return _prevClosePx; }
	/**
	 * @brief Set the Calendar Days Since Prev Trading Day object
	 * 
	 * @param days 
	 */
	void setCalendarDaysSincePrevTradingDay(int days) { _calendarDaysSincePrevTradingDay = days; }
	/**
	 * @brief get the calendar Days Since  Prev Trading Day
	 * 
	 * @return int 
	 */
	int calendarDaysSincePrevTradingDay() { return _calendarDaysSincePrevTradingDay; }

	inline void setOrderBookManager(IOrderBookManager *orderBookMgr) { _orderBookMgr = orderBookMgr; }
	inline IOrderBookManager *getOrderBookManager() { return _orderBookMgr; }
	inline LevelAccessor &getBookLevelAccessor(TradeDirs side) { return _orderBookMgr->getLevelAccessor(side); }

	double calcBookQty(TradeDirs side, double deepestPrice, bool inclusive = true);

	double getLevelPrice(TradeDirs side, int level, bool isPrevBook = false) const;
	double getLevelQty(TradeDirs side, int level, bool isPrevBook = false) const;
	int getLevelOrds(TradeDirs side, int level, bool isPrevBook = false) const;
	double getQtyOnLevels(TradeDirs side, double deepestPrice, bool inclusive = true, bool isPrevBook = false) const;
	double getPrevQtyOnLevels(TradeDirs side, double deepestPrice, bool inclusive = true) const;

	/**
	 * @brief Get the Venue Local Symbol Name
	 * 
	 * @return string 
	 */
	inline string getLocalSymbolName() const { return local_name; }
	inline const string getCoin() const {return coin;}
	inline const string getBasecoin() const {return base_coin;}
	inline string getProductName() const { return product_name; }

	void setConfig(json &cfg) { _cfg = cfg; }
	json &getConfig() { return _cfg; }

	inline bool isUsdFutures() const { return asset_class == AssetClass::CC_USD_FUTURES; }
	/**
	 * @brief is a contract usdtbased futures?
	 * 
	 * @return true 
	 * @return false 
	 */
	bool isUsdtFutures() const { return asset_class == AssetClass::FUTURES; }
	/**
	 * @brief is Trading partners crypto spot?
	 * 
	 * @return true 
	 * @return false 
	 */
	bool isCryptoSpot() const { return asset_class == AssetClass::CC; }
	/**
	 * @brief calulate trades notional value
	 * 
	 * @param qty 
	 * @return double 
	 */
	double calcNotional(double qty) const { return qty * contract_multiplier * (isUsdFutures() ? 1.0 : _risk_px); }
	double calcNotional(double qty, double px) const { return qty * contract_multiplier * (isUsdFutures() ? 1.0 : px); }
	double qtyFromNotional(double notional) const { return notional / contract_multiplier / (isUsdFutures() ? 1.0 : _risk_px); }
	double qtyFromNotional(double notional, double px) const { return notional / contract_multiplier / (isUsdFutures() ? 1.0 : px); }

	bool validate_trade(double price, double qty) const;

public:
	json _cfg;
	uint64_t cid = -1;
	string ticker;
	string exchange;
	string local_name;
	string coin;
	string base_coin;
	string product_name;

	// options
	string underlying;
	string expiry;
	double strike;
	bool is_call;

	bool active = true;
	bool trade = false;
	double tick_size = 0.01;
	double lot_size = 1;
	double min_order_size = 1.0;
	double contract_multiplier = 1;
	int qty_precision = 0;
	int price_precision = 2;
	int lever = 1;
	AssetClass asset_class;

	double _prevClosePx = std::numeric_limits<double>::quiet_NaN();
	int _calendarDaysSincePrevTradingDay = 1;

	double trade_px = 0;
	double trade_qty = 0;
	int trade_type = 0;
	double volume = 0;
	double bid_px = 0;
	double ask_px = 0;
	double bid_qty = 0;
	double ask_qty = 0;
	int64_t bid_ords = 0;
	int64_t ask_ords = 0;
	nanoseconds quote_time = nanoseconds(0);
	nanoseconds trade_time = nanoseconds(0);
	double bid_tbbo = 0;
	double ask_tbbo = 0;

	double bid_xbbo = 0;
	double ask_xbbo = 0;
	double bid_xbbo_qty = 0;
	double ask_xbbo_qty = 0;

	// double _open_vwap = 0.0;
	double _open_mid_avg = 0.0;
	double _mid_px = 0.0;
	double _spread = 0.0;
	double _risk_px = 0.0;
	bool _hasRiskPrice = false;
	bool _stale = false;
	bool _ready = false;

	bool _useIBdata = false; // ib qty is in lot !!!

	IOrderBookManager *_orderBookMgr = nullptr;
};
