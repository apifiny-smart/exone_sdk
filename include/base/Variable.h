#pragma once
#include "base/JsonConfigurable.h"
#include "base/Notifiable.h"
#include "base/PricingModel.h"
#include "base/SamplerListener.h"
#include "base/SymbolListener.h"
#include "base/TimerListener.h"
#include "base/utils.h"

using namespace std;

typedef vector<double> SignalComponents;
/**
 * @brief contains the set of features the strategy makes use of. A host of alpha signals and logical rules (AND, OR, NOT etc.) can be defined in a hierarchical or independent way, which ultimately feeds into the strategy model
 * 
 */
class Variable: public virtual NotifiableTree, public SymbolListener, public SamplerListener, public TimerListener, public JsonConfigurable {
public:
	using JsonConfigurable::JsonConfigurable;
	void onTimer(TimerHandler* th, long msecs) override {}
	/**
	 * @brief The sample event callback triggers the update of the variable
	 * 
	 * @param src Sampler object
	 * @param cnt OnSample will be triggered once every few times, the default is 1
	 * @param decay weight of new data:（1-decay）
	 */
	void onSample(Sampler* src, int cnt, double decay) override {}
	/**
	 * @brief The symbol quote update callback triggers the update of the variable
	 * 
	 * @param cid Unique identifier for symbol
	 * @param si symbolinfo
	 */
	void onQuote(long cid, const SymbolInfo& si) override {}
	/**
	 * @brief trigger callback on tick update
	 * 
	 * @param cid 
	 * @param si 
	 */
	void onTick(long cid, const SymbolInfo& si) override {}
	/**
	 * @brief If the marketupdate is not received within 10 minutes, the status of the symbol will be set to unready
	 * 
	 * @param cid Unique identifier for symbol
	 * @param si  symbolinfo
	 */
	void onQuoteStale(long cid, const SymbolInfo& si) override { _ready = false; setUpdated(); }
	/**
	 * @brief  get the variable value
	 * 
	 * @return double 
	 */
	virtual double value() { return _value; }
	/**
	 * @brief is the variable ready for trading
	 * 
	 * @return true 
	 * @return false 
	 */
	virtual bool is_ready() { return _ready; }
	/**
	 * @brief Get the variable that the signal depends on
	 * 
	 * @param comps 
	 */
	virtual void getSignalComponents(SignalComponents& comps) {}

protected:
	double _value = 0;
	bool _ready = false;
};

typedef pair<double, Variable*> WeightedVariable;
typedef vector<WeightedVariable> WeightedVariableVector;
