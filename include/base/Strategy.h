#pragma once
#include "base/SymbolNotifiableTree.h"
#include "base/JsonConfigurable.h"
#include "base/SymbolListener.h"
#include "base/TimerListener.h"
/**
 * @brief base class for strategy class,which can inherit the logic used to implement strategy
 * 
 */
class Strategy: public virtual SymbolNotifiableTree,
				public SymbolListener,
				public TimerListener,
				public JsonConfigurable {
public:
	using JsonConfigurable::JsonConfigurable;
	/**
	 * @brief Set the Active status
	 * 
	 * @param active 
	 */
	void setActive(bool active) { _active = active; }
	
	virtual SymbolInfo *getSymbolInfo() = 0;
	/**
	 * @brief trigger callback on received cmd
	 * 
	 * @param cmd 
	 */
	virtual void onCommand(json& cmd) = 0;
	// virtual string& getTradeMarket() = 0;
	/**
	 * @brief is use margin? the value is false for spot 
	 * 
	 * @return true 
	 * @return false 
	 */
	virtual bool isUseMargin() = 0;
	/**
	 * @brief Is it a cross margin mode or an isolated margin mode 
	 * 
	 * @return string& 
	 */
	virtual string& marginSource() = 0;

protected:
	bool _active = true;
};

