#pragma once
#include "base/Notifiable.h"

class Sampler;

class SamplerListener: public virtual NotifiableTree {
public:
	/**
	 * @brief Destroy the Sampler Listener object
	 * 
	 */
	virtual ~SamplerListener() {}
	/**
	 * @brief trigger callback onsample
	 * 
	 * @param src 
	 * @param cnt 
	 * @param decay 
	 */
	virtual void onSample(Sampler* src, int cnt, double decay) = 0;
};
