#pragma once
#include "common/common.h"
#include <set>
using namespace std;

typedef set<long> CidSet;

class Notifiable {
public:
	virtual ~Notifiable() {}
	virtual void notify(long id) = 0;
	virtual void setUpdated() = 0;
	//virtual bool hasUpdate() = 0;
	virtual void addParent(Notifiable *p) = 0;
	virtual void addSymbolToNotify(int cid) = 0;
	virtual CidSet getSymbolsToNotify() = 0;
};

class NotifiableLeaf : public virtual Notifiable {
public:
	NotifiableLeaf() {}

	virtual void onNotify() {}

	void notify(long id = 0) override {
		//if(id > _lastNoticeId) {
			if(_hasUpdate) {
				onNotify();
				_hasUpdate = false;
			}
			_lastNoticeId = id;
		//}
	}

	void addParent(Notifiable *p) override {
		_parents.push_back(p);
	}

	void setUpdated() override {
		_hasUpdate = true;
		for(auto& p: _parents)
			p->setUpdated();
	}
	//bool hasUpdate() override { return _hasUpdate; }

	void addSymbolToNotify(int cid) override {
		_cids.insert(cid);
	}

protected:
	CidSet getSymbolsToNotify() override { return _cids; }

protected:
	bool _hasUpdate = true; //initialized to ture to make sure that onNotify of all nodes get called at least once. 
	long _lastNoticeId = 0;
	CidSet _cids;

	vector<Notifiable *> _parents;
};

class NotifiableTree : public virtual NotifiableLeaf {
public:
	void notifyChildren(long id = 0) {
		for(auto& c: _notifiableChildren) {
			c->notify(id);
		}
	}

	void notify(long id = 0) override {
		//if(id > _lastNoticeId) {
			if(_hasUpdate) {
				notifyChildren(id);
				onNotify();
				_hasUpdate = false;
			}
			_lastNoticeId = id;
		//}
	}
	void addNotifiableChild(Notifiable* n) {
		assert(n != nullptr);
		if( std::find(_notifiableChildren.begin(), _notifiableChildren.end(), n) == _notifiableChildren.end()) {
			_notifiableChildren.push_back(n);
			n->addParent(this);
		}
	}

protected:

	CidSet getSymbolsToNotify() override {
		CidSet allCids;
		for(auto& c: _notifiableChildren) {
			CidSet ccids = c->getSymbolsToNotify();
			allCids.insert(ccids.begin(), ccids.end());
		}
		allCids.insert(_cids.begin(), _cids.end());
		return allCids;
	}

protected:

	vector<Notifiable *> _notifiableChildren;
};


class SelectiveNotifiableTree : public virtual NotifiableTree {
public:
	virtual void notify(long id, bool force=false) {
		if(force || _needNotify) {
			NotifiableTree::notify(id);
			_needNotify = false;
		}
	}

	void setNeedNotify() {
		_needNotify = true;
	}
private:
	bool _needNotify = false;
};
