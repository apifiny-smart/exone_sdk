#pragma once
#include "common/common.h"
#include "base/MarketMessage.h"
/**
 * @brief Mainly used for importing data and other information
 * 
 */
class FeedParser {
public:
	
	virtual ~FeedParser() {}
	/**
	 * @brief Base class for parsing raw data, such as market data from okex exchange
	 * 
	 * @param action 'P':FullDepthMessage;'U':DeltaDepthMessage;'T':TradeMessage
	 * @param data raw data
	 * @return MarketMessage* 
	 */
	virtual MarketMessage* parseRawMessage(char action, const string& data) = 0;
};

