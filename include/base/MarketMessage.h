#pragma once
#include "common/common.h"
#include "components/SymbolManager.h"
#include "base/BookUpdater.h"
#ifndef NO_PB
#include "pbgen18/TradeResp.pb.h"
#endif


class OrderCreateResp;
class OrderCancelResp;

class MsgData {
public:
	virtual ~MsgData() {}
	string rawMsg;
};
/**
 * @brief the class of trades update
 * 
 */
class TradeUpdate: public MsgData {
public:
	static const int AGGR_IS_BUY = 1;
	static const int AGGR_IS_SELL = -1;
	static const int AGGR_UNKNOWN = 0;

	TradeUpdate() {}
	/**
	 * @brief Construct a new Trade Update object
	 * 
	 * @param p 
	 * @param q 
	 * @param t aggressive or passive trading
	 */
	TradeUpdate(double p, double q, int t) : price(p), qty(q), trade_type(t) {}

public:
	double price;
	double qty;
	/**
	 * @brief aggressive or passive trading,{-1, 0, 1}, 1 means aggressor side is BUY
	 * 
	 */
	int trade_type;
	long ts;
};
/**
 * @brief the class of depth update
 * 
 */
class DepthUpdate: public MsgData {
public:
	DepthUpdate(int t) : updateType(t) {}
	int updateType;
};

class CCJsonUpdate: public MsgData {
public:

	CCJsonUpdate() {}
	CCJsonUpdate(json& msg) : msg(msg) {}
public:
	json msg;
};

class CBalanceData : public MsgData{	
public:
	CBalanceData() {}
	// CBalanceData(string venue,string symbol,double balance):_venue(venue),_symbol(symbol),_balance(balance){}

public:
	double _balance;
	string _venue;
	string _symbol;
	string _currency;
	string _type;
};

class CPositionData : public MsgData{	
public:
	CPositionData() {}
	// CPositionData(string venue,string symbol,double position):_venue(venue),_symbol(symbol),_position(position){}

public:
	double _position;
	string _venue;
	string _type;
	string _symbol;
};

class CVolatilityData : public MsgData{	
public:
	/**
	 * @brief Construct a new CVolatilityData object
	 * 
	 */
	CVolatilityData() {}
	// CVolatilityData(string venue,string symbol,double position):_venue(venue),_symbol(symbol),_position(position){}

public:
	double _value;
	string _venue;
	string _type;
	string _symbol;
};

class COrderData:public MsgData{
public:
	COrderData(){}	
public:	
	long _timestamp = 0;
	long _order_id = 0;
	string _order_id_str;
	double _filled_volum = 0.0;
	double _filled_price = 0.0;
	double _filled_notional = 0.0;
	double _last_filled_volum = 0.0;
	double _last_filled_price = 0.0;
	double _average_price = 0.0;
	Liquidity _liquidity;
	string _venue;
	string _symbol;
	string _status;	
};

#ifndef NO_PB
class CTradeOrderData:public MsgData{
public:
	CTradeOrderData(Apifiny::TradeRespMsg* t){
		_trade=t;
	}	
	~CTradeOrderData(){
		if(_trade)
			delete _trade;
	}
public:	
	Apifiny::TradeRespMsg* _trade = NULL;
};
#endif

class CCreateOrderRespData:public MsgData{
public:
	CCreateOrderRespData(OrderCreateResp* t){
		_data=t;
	}	
	~CCreateOrderRespData(){
	}
public:	
	OrderCreateResp *_data = NULL;
};

class CCancelOrderRespData:public MsgData{
public:
	CCancelOrderRespData(OrderCancelResp* t){
		_data=t;
	}	
	~CCancelOrderRespData(){
	}
public:	
	OrderCancelResp *_data = NULL;
};



class MarketMessage {
public:
	enum MessageTypes {
		DEPTH,
		TRADE,
		SimTIMER,
		SimDEPTH,
		SimTRADE,
		TRADE_CHANNEL,
		ORDER, // order
		BANLANCE,
		POSITION,
		CreateOrderResp,
		CancelOrderResp,
		VOLATILITY

	};

	MarketMessage() {}
	MarketMessage(MessageTypes t) : type(t) {}
	MarketMessage(MessageTypes t, MsgData* d) : type(t), data(d) {}
	MarketMessage(int c, MessageTypes t, MsgData* d) : cid(c), type(t), data(d) {}
	virtual ~MarketMessage() { if(data) delete data;}

	MarketMessage &operator=(const MarketMessage& msg)
	{
		this->cid = msg.cid;
		this->type = msg.type;
		this->data = msg.data;

		return *this;
	}

public:
	int cid;
	MessageTypes type;
	nanoseconds arriveTime;
	nanoseconds enqueueTime;
	MsgData* data = NULL;
	
};

class MarketMessageHub {
public:
	virtual ~MarketMessageHub() {}
	virtual void enqueueMarketMessage(MarketMessage* msg) = 0;
};
