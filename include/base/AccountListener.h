#pragma once
#include "base/SymbolInfo.h"
#include "base/Order.h"

class Account;
/**
 * @brief Used to monitor account changes
 * 
 */
class AccountListener {
public:
	
	virtual ~AccountListener() {}
	/**
	 * @brief Triggered callback on set position,able to trigger publishRisk
	 * 
	 * @param acct account object
	 * @param cid the unique identifier of the symbol
	 * @param oldPos old posiitons
	 * @param pos  new positions
	 */
	virtual void onPositionSet(Account* acct, long cid, double oldPos, double pos) = 0;
	/**
	 * @brief Triggered callback on risk update,able to trigger publishRisk
	 * 
	 * @param acct account object
	 * @param riskId Unique identifier for risk pairing
	 * @param oldRisk 
	 * @param risk 
	 * @param localRiskChanged 
	 */
	virtual void onRiskUpdate(Account* acct, int riskId, double oldRisk, double risk, bool localRiskChanged) = 0;
};

