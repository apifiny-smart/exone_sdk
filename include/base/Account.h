#pragma once
#include "common/common.h"
#include "base/AccountListener.h"
#include "base/JsonConfigurable.h"
 
class SymPos {
public:
	double cash = 0;
	double position = 0;
	double notional = 0;
	double pos_px = 0; //for USD futures
};
/**
 * @brief the map cached symbol position
 * 
 */
typedef unordered_map<long, SymPos> SymPosMap;
/**
 * @brief The user account contains information such as the position, notional, risk and other information of each symbol
 * 
 */
class Account: public JsonConfigurable {
public:
	using JsonConfigurable::JsonConfigurable;
	virtual long accountId() = 0;
	virtual void addListener(AccountListener* l) = 0;
	/**
	 * @brief Absolute value of account notional value
	 * 
	 * @return double 
	 */
	virtual double absNotional() = 0;
	/**
	 * @brief Notional value of account positive positions
	 * 
	 * @return double 
	 */
	virtual double posNotional() = 0;
	/**
	 * @brief Notional value of account negetive positions
	 * 
	 * @return double 
	 */
	virtual double negNotional() = 0;

	virtual void setPosition(long cid, double pos, double pnl, double avgFillPrice) = 0;
	virtual double position(long cid) = 0;
	/**
	 * @brief Average position price
	 * 
	 * @param cid the unique identifier of the symbol
	 * @return double 
	 */
	virtual double pos_px(long cid) = 0;
	virtual double cash(long cid) = 0;
	virtual double notional(long cid) = 0;
	virtual SymPos& sympos(long cid) = 0;
	virtual double calcPnl(long cid, double px) = 0;
	/**
	 * @brief The number of risk pairs in the account
	 * 
	 * @return int 
	 */
	virtual int numRisks() = 0;
	virtual vector<double>& localRisks() = 0;
	virtual vector<double>& risks() = 0;
	virtual double localRisk(int riskId) = 0;
	/**
	 * @brief The risk value of a specific risk pair
	 * 
	 * @param riskId Unique identifier for risk pairing
	 * @return double 
	 */
	virtual double risk(int riskId) = 0;
	/**
	 * @brief The risk multiplier for a symbol in the risk pairing
	 * 
	 * @param riskId Unique identifier for risk pairing
	 * @param cid the unique identifier of the symbol
	 * @return double 
	 */
	virtual double getRiskMultiple(int riskId, long cid) = 0;

	virtual double absRisk(int riskId) = 0;
	/**
	 * @brief How many types of symbols are in a risk pairing
	 * 
	 * @param riskId Unique identifier for risk pairing
	 * @return int 
	 */
	virtual int symbolCount(int riskId) = 0;
	
	virtual void setMyRiskInstId(long instId) = 0;
	virtual vector<long>& riskSharingInstances() = 0;
	/**
	 * @brief Set the risk value of a risk pair in an instance
	 * 
	 * @param riskId Unique identifier for risk pairing
	 * @param instId Unique identifier for instance
	 * @param risk 
	 */
	virtual void setInstanceRisk(int riskId, long instId, double risk) = 0;
	
	// called by BaseStrategy
	virtual void onOrderExec(SymbolInfo* si, TradeDirs side, double px, double qty, Liquidity liq, LOrder* order) = 0;
	virtual void onOrderAcked(SymbolInfo* si, LOrder* order) = 0;
	virtual void onOrderClosed(SymbolInfo* si, LOrder* order) = 0;
	virtual void onOrderCanceled(SymbolInfo* si, LOrder* order) = 0;
	virtual void onOrderRejected(SymbolInfo* si, LOrder* order) = 0;
	virtual void onOrderCancelRejected(SymbolInfo* si, LOrder* order) = 0;
};
