#pragma once
#include "base/Consts.h"
#include "common/common.h"

/**
 * @brief the opposite side of trade
 * 
 * @param side 
 * @return TradeDirs 
 */
inline TradeDirs oppositeSide(TradeDirs side) {
	return (side==SELL) ? BUY : (side==BUY) ? SELL: NUM_TRADE_DIRS;
}
/**
 * @brief price1 is better than price2?
 * 
 * @param p1 
 * @param p2 
 * @param side 
 * @return true 
 * @return false 
 */
inline bool priceBetterThan(double p1, double p2, TradeDirs side) {
	return (side==BUY) ? miscgt(p1, p2) : miscgt(p2, p1);
}
/**
 * @brief price1 is worse than price2?
 * 
 * @param p1 
 * @param p2 
 * @param side 
 * @return true 
 * @return false 
 */
inline bool priceWorseThan(double p1, double p2, TradeDirs side) {
	return (side==BUY) ? misclt(p1, p2) : misclt(p2, p1);
}
/**
 * @brief price1 is equal price2?
 * 
 * @param p1 
 * @param p2 
 * @return true 
 * @return false 
 */
inline bool priceEquals(double p1, double p2) {
	return misceq(p1, p2);
}
/**
 * @brief price to promote make a deal
 * 
 * @param px 
 * @param deltaPx 
 * @param side 
 * @return double 
 */
inline double improvePrice(double px, double deltaPx, TradeDirs side) {
	return (side==BUY) ? px + deltaPx : px - deltaPx;
}
/**
 * @brief price to do not promote make a deal
 * 
 * @param px 
 * @param deltaPx 
 * @param side 
 * @return double 
 */
inline double worsenPrice(double px, double deltaPx, TradeDirs side) {
	return (side==BUY) ? px - deltaPx : px + deltaPx;
}
/**
 * @brief get the price to do not promote make a deal from 2 prices
 * 
 * @param px1 
 * @param px2 
 * @param side 
 * @return double 
 */
inline double pickWorsePrice(double px1, double px2, TradeDirs side) {
	bool px1Greater = px1 > px2;
	return (side==BUY) ?
			(px1Greater ? px2 : px1) :
			(px1Greater ? px1 : px2);
}
/**
 * @brief get the price to promote make a deal from 2 prices
 * 
 * @param px1 
 * @param px2 
 * @param side 
 * @return double 
 */
inline double pickBetterPrice(double px1, double px2, TradeDirs side) {
	bool px1Greater = px1 > px2;
	return (side==BUY) ?
			(px1Greater ? px1 : px2) :
			(px1Greater ? px2 : px1);
}
