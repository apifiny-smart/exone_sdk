#pragma once
#include "base/Notifiable.h"
#include "base/SymbolListener.h"
#include "components/SymbolManager.h"
/**
 * @brief this class is used to manage the notify tree for updating symbols
 * 
 */
class SymbolNotifiableTree: public virtual SelectiveNotifiableTree, public SymbolUpdateListener {
public:
	virtual void after_init() {
		/**
		 * @brief Construct a new regsiter For Symbol Udpates object
		 * 
		 */
		regsiterForSymbolUdpates();
	}
	/**
	 * @brief trigger callback on symbol update
	 * 
	 */
	void onSymbolUpdate() {
		setNeedNotify();
	}

	void regsiterForSymbolUdpates() {
		SymbolManager* symMgr = SymbolManager::instance();
		CidSet cids = getSymbolsToNotify();
		for(auto cid : cids) {
			SymbolHandler* sh = symMgr->getSymbolHandler(cid);
			sh->addUpdateListener(this);
		}
	}
};
