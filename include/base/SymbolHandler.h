#pragma once
#include "base/SymbolListener.h"
#include "common/common.h"
//#include "client/Contract.h"
/**
 * @brief Symbol management class, manages various types of monitoring, data update callbacks
 * 
 */
class SymbolHandler {
public:
	SymbolHandler(long cid, const string& ticker, const string& exch);

	SymbolInfo& symbolInfo() { return _si; }
	long cid() { return _si.cid; }
	bool active() { return _si.active; }
	
	bool match_symbol(const string& ticker, const string& exch);
	/**
	 * @brief L1 Change and trades, process first
	 * 
	 * @param l 
	 */
	void addFastListener(SymbolFastListener *l); 
	/**
	 * @brief L1 Change and trades
	 * 
	 * @param l 
	 */
	void addListener(SymbolListener *l); 
	/**
	 * @brief L2 Change
	 * 
	 * @param l 
	 */
	void addL2ChangeListener(L2ChangeListener *l); 
	void addUpdateListener(SymbolUpdateListener *l);
	/**
	 * @brief trigger callback by quote update,process first
	 * 
	 */
	void fireOnQuoteFast();
	/**
	 * @brief trigger callback by tick update,process first
	 * 
	 */
	void fireOnTickFast();
	/**
	 * @brief trigger callback by quote update
	 * 
	 */
	void fireOnQuote();
	/**
	 * @brief trigger callback by tick update
	 * 
	 */
	void fireOnTick();
	/**
	 * @brief trigger callback by l2 update
	 * 
	 */
	void fireOnL2Change();
	/**
	 * @brief If the marketupdate is not received within 10 minutes, the status of the symbol will be set to unready,check the symbol status
	 * 
	 */
	void checkStaleness();

protected:
	/**
	 * @brief process on quote update
	 * 
	 */
	void updateOnQuote();
	/**
	 * @brief process on tick update
	 * 
	 */
	void updateOnTick();
	/**
	 * @brief process on l2 update
	 * 
	 */
	void updateOnL2Change();

private:
	SymbolInfo _si;
	vector<SymbolFastListener *> _fastListeners;
	vector<SymbolListener *> _listeners;
	vector<L2ChangeListener *> _l2ChangeListeners;
	vector<SymbolUpdateListener *> _updateListeners;

	//nanoseconds _openVwapStartTime;
	//nanoseconds _openVwapEndTime;
	//double _openVwapNotional = 0.0;
	//double _openVwapShares = 0;

	double _open_mid_sum = 0;
	int _open_mid_sum_samples = 0;
	double _prev_mid_px = 0.0;

	double _max_valid_spread = 10.0;

	bool _applyOpenPeriod = false;
	nanoseconds _init_price_time = nanoseconds(0);
	nanoseconds _nextInitPxSampleTime;
	nanoseconds _openPeriodEndTime;

	nanoseconds _stale_threshold = nanoseconds(300000000000L);
};
