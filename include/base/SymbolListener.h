#pragma once

#include "base/SymbolInfo.h"
#include "base/Notifiable.h"
/**
 * @brief The listener class of symbol, used to monitor market updates
 * 
 */
class SymbolListener: public virtual Notifiable {
public:
	virtual ~SymbolListener() {}
	/**
	 * @brief trigger callback on quote update
	 * 
	 * @param cid 
	 * @param si 
	 */
	virtual void onQuote(long cid, const SymbolInfo& si) = 0;
	/**
	 * @brief trigger callback on tick update
	 * 
	 * @param cid 
	 * @param si 
	 */
	virtual void onTick(long cid, const SymbolInfo& si) = 0;
	/**
	 * @brief If the marketupdate is not received within 10 minutes, the status of the symbol will be set to unready
	 * 
	 * @param cid the unique identifier of the symbol
	 * @param si 
	 */
	virtual void onQuoteStale(long cid, const SymbolInfo& si) = 0;
};

class SymbolUpdateListener {
public:
	virtual ~SymbolUpdateListener() {}
	/**
	 * @brief trigger callback on symbol update
	 * 
	 */
	virtual void onSymbolUpdate() = 0;
};

class L2ChangeListener {
public:
	virtual ~L2ChangeListener() {}
	/**
	 * @brief trigger callback on l2 update
	 * 
	 * @param cid 
	 * @param si 
	 */
	virtual void onL2Change(long cid, const SymbolInfo& si) = 0;
};

class SymbolFastListener {
public:
	virtual ~SymbolFastListener() {}
	/**
	 * @brief trigger callback on quote update,process first
	 * 
	 * @param cid 
	 * @param si 
	 */
	virtual void onQuoteFast(long cid, const SymbolInfo& si) = 0;
	/**
	 * @brief trigger callback on tick update
	 * 
	 * @param cid 
	 * @param si 
	 */
	virtual void onTickFast(long cid, const SymbolInfo& si) = 0;
};
