#pragma once
#include <string>
#include <iostream>
#include <sstream>
#include <sys/socket.h>
#include <list>
#include <string.h>
#include <stddef.h>

using namespace std;

namespace PB
{
    class ByteBuffer : std::stringbuf
    {
    public:
        ByteBuffer(int size)
        {
            _write_index = 0;
            _read_index = 0;
            _size = size;
            _buffer =  (char*)malloc(sizeof(char) * size);
        }
        ~ByteBuffer()
        {
            if(_buffer)
                free(_buffer);
        }

        void reset()
        {
            _write_index = 0;
            _read_index = 0;
        }

        int get_int()
        {
            if (read_able() < 4)
                return 0;

            int size = ((u_char)_buffer[_read_index] << 24 | (u_char)_buffer[_read_index + 1] << 16 | (u_char)_buffer[_read_index+2] << 8 | (u_char)_buffer[_read_index+3]);

            _read_index += sizeof(int);
            
            return size;
        }

        inline int get(char *out, int count)
        {
            if(read_able() < count)
                return 0;

            memcpy(out,(const char*)(_buffer + _read_index),count);

            _read_index += count;
            return count;
        }

        inline int put(char *out, int count)
        {
            if(write_able() < count)
                return 0;

            memcpy(_buffer + _write_index,out,count);
            _write_index += count;

            return count;
        }

        inline int read_able(){
            if(_write_index > _read_index)
                return _write_index - _read_index;
            return 0;
        }

        inline int write_able(){            
            return (_size - _write_index);
        }

        inline int write_index(){return _write_index;}
        inline int read_index(){return _read_index;}

    private:
        int _size;
        int _write_index;
        int _read_index;
        char* _buffer;
    };

    class Session
    {
    public:
        Session();
        ~Session();

    public:
        virtual int on_recv(char *in, int size);
        virtual int on_send(char *out, int size);
        virtual void onBreak(){};
        virtual int subscribe();

        bool connect(string ip, int port);
        int recv();
        int send_pb(char *out, int size);
        int close();
        inline void set_timeout(int time) { _timeout = time; }

    protected:
        bool start;
        bool is_connect;
        int send(char *out, int size);
        bool reconnect();
        void split();

        int sockfd, fdsocket;
        string ip;
        int port;
        int _timeout;
        int _the_last_package_size;
        ByteBuffer* _recv_buffer;
        char* _temp_buffer;
    };
}