#pragma once

#include <future>
#include <thread>
#include <boost/lockfree/queue.hpp>
#include <boost/lockfree/queue.hpp>
#include <stdlib.h>
#include <signal.h>
// #include "hiredis/hiredis.h"
// #include "hiredis/async.h"
// #include "hiredis/adapters/libevent.h"
#include "common/common.h"
#include "common/concurrentqueue.h"
#include "common/blockingconcurrentqueue.h"
#include "components/SymbolManager.h"
#include "components/TimerManager.h"
#include "components/StrategyManager.h"
#include "components/StrategyManager.h"
#include "components/PlayerManager.h"
#include "components/NotificationManager.h"
#include "components/BalanceManager.h"
#include "components/VolatilityManager.h"
#include "components/ContractPositionsManager.h"
#include "base/TradeApi.h"
#include "base/Order.h"
#include "base/AccountListener.h"
#include "base/DataTypes.h"
#include "base/MarketMessage.h"
#include "base/BaseTrader.h"
// #include "CTemplation.h"

#define EXCHANGE_DEFINE(name)                                   \
    LOrderMap _##name##OrderMap;                                \
    LOrderDataMap _##name##UnknowOrderUpdateCache;              \
	SOrderMap _##name##OrderMap_S;								\
	SOrderDataMap _##name##UnknowOrderUpdateCache_S;            \
    void init##name(json &cfg);                                 \
    void process##name##OrderUpdate(MarketMessage *msg);        \
    void process##name##CachedOrderUpdates(long remoteOrderId); \
    void process##name##CachedOrderUpdates(std::string remoteOrderId); \
    name##MarketHandler *_##name##MarketHandler = nullptr;  \
    name##TradeHandler *_##name##TradeHandler = nullptr;

using namespace moodycamel;
#define SLOG cout << toStr(now())

class redisContext;
class redisAsyncContext;
class SymbolTrader;
class CTradeOrderData;
class Message;
class AccountManager;
class CobSessionHandle;
class TraderCob;
class BitmexMarketHandler;
class HuobiTradeHandler;
class BinanceSpotMarketHandler;
class BinanceSpotTradeHandler;
class BinanceSwapMarketHandler;
class BinanceSwapTradeHandler;
class BinanceUsMarketHandler;
class BinanceUsTradeHandler;
class IBL2MarketHandler;
class CobMarketHandler;
class FeedParser;
class SimMarket;
class HuobiSwapTradeHandler;
class HuobiSwapMarketHandler;
class CoinbaseSpotMarketHandler;
class CoinbaseSpotTradeHandler;
class CoinbaseSpotMarketHandler;
class DeribitSwapTradeHandler;
class DeribitSwapMarketHandler;
class DeribitOptionTradeHandler;
class DeribitOptionMarketHandler;
namespace Apifiny
{
	class TradeRespMsg;
	class Balance;
	class ContractPosition;
};

namespace OKEX_V5
{
	class OkexSpotMarketHandler;
	class OkexSpotTradeHandler;
	class OkexSwapTradeHandler;
	class OkexSwapMarketHandler;
	class OkexFuturesMarketHandler;
	class OkexFuturesTradeHandler;
};

namespace HUOBI_V2
{
	class HuobiMarketHandler;
};

namespace OKCOIN
{
	class OkcoinSpotMarketHandler;
	class OkcoinSpotTradeHandler;
};

namespace FTX
{
	class FtxSpotMarketHandler;
	class FtxSpotTradeHandler;
	class FtxSwapMarketHandler;
	class FtxSwapTradeHandler;
};


class CCTrader : public TradeApi, public MarketMessageHub, public AccountListener, public BaseTrader
{
	/**
	 * @brief event message 
	 * 
	 */
	class EventTimeMessage
	{
	public:
		enum MessageTypes
		{
			DEPTH,
			TRADE,
			NOTICE
		};
		EventTimeMessage() {}
		virtual ~EventTimeMessage() {}
		int msgType;
		long nextNoticeId;
		nanoseconds eventTime;
		nanoseconds enqueueTime;
		nanoseconds arriveTime;
	};

	class RedisPubMessage
	{
	public:
		enum MessageTypes
		{
			CMD_RESP,
			HEART_BEAT,
			MODEL_STATS,
			APP_STATS,
			RISK_PUB
		};
		RedisPubMessage() {}
		RedisPubMessage(int t) : msgType(t) {}
		virtual ~RedisPubMessage() {}
		int msgType;
	};

	class CmdRespMessage : public RedisPubMessage
	{
	public:
		CmdRespMessage() : RedisPubMessage(CMD_RESP) {}
		CmdRespMessage(json *d) : RedisPubMessage(CMD_RESP), data(d) {}
		~CmdRespMessage()
		{
			if (data)
				delete data;
		}
		json *data = NULL;
	};

	class HeartbeatMessage : public RedisPubMessage
	{
	public:
		HeartbeatMessage() : RedisPubMessage(HEART_BEAT) {}
		HeartbeatMessage(const string &i, const string &s) : RedisPubMessage(HEART_BEAT), instance(i), state(s) {}
		string instance;
		string state;
	};

	class ModelStatsMessage : public RedisPubMessage
	{
	public:
		ModelStatsMessage() : RedisPubMessage(MODEL_STATS) {}
		ModelStatsMessage(const string &i, json *s) : RedisPubMessage(MODEL_STATS), instance(i), stats(s) {}
		~ModelStatsMessage()
		{
			if (stats)
				delete stats;
		}
		string instance;
		json *stats = NULL;
	};

	class AppStatsMessage : public RedisPubMessage
	{
	public:
		AppStatsMessage() : RedisPubMessage(APP_STATS) {}
		AppStatsMessage(const string &i, json *s) : RedisPubMessage(APP_STATS), appinst(i), stats(s) {}
		~AppStatsMessage()
		{
			if (stats)
				delete stats;
		}
		string appinst;
		json *stats = NULL;
	};

	class RiskPubMessage : public RedisPubMessage
	{
	public:
		RiskPubMessage() : RedisPubMessage(RISK_PUB) {}
		RiskPubMessage(long acctId_, int risId_, long instId_, double risk_)
			: RedisPubMessage(RISK_PUB), acctId(acctId_), riskId(risId_), instId(instId_), risk(risk_) {}
		long acctId;
		int riskId;
		long instId;
		double risk;
	};

	typedef unordered_map<long, list<json>> JsonListMap;
	typedef ConcurrentQueue<OrderResp *> OrderRespQueue;
	typedef ConcurrentQueue<MarketMessage *> MarketMessageQueue;
	typedef BlockingConcurrentQueue<EventTimeMessage *> EventTimeMessageBQueue;
	typedef BlockingConcurrentQueue<RedisPubMessage *> RedisPubMessageBQueue;
	typedef unordered_map<int, SymbolTrader *> SymbolTraderMap;
	typedef unordered_map<string, list<Apifiny::TradeRespMsg *>> TradeRespListMap;

public:
	CCTrader(json &cfg);
	void loadXlibs();

	void initialize();
	void run();
	void async_run();


	/**
	 * @brief enqueue MarketMessage to _marketMsgQueue
	 * 
	 * @param msg 
	 */
	void enqueueMarketMessage(MarketMessage *msg) override;

	/**
	 * @brief enqueue order response message  to _orderRespQueue
	 * 
	 * @param msg 
	 */
	void enqueueOrderResp(OrderResp *msg) override;

	// implement TradeApi
	LOrder *sendOrder(LOrder *order) override;
	void cancelOrder(LOrder *order) override;
	/**
	 * @brief Report received commands to redis
	 * 
	 * @param data 
	 */
	void publishCmdResp(json *data) override;
	/**
	 * @brief Report heartbeat to redis
	 * 
	 * @param inst instance
	 * @param state  status
	 */
	void publishHeartBeat(const string &inst, const string &state) override;
	/**
	 * @brief Report model status to redis
	 * 
	 * @param inst instance
	 * @param stats status
	 */ 
	void publishModelStats(const string &inst, json *stats) override;

	// implement IOrderManager
	/**
	 * @brief ??
	 * 
	 * @param order 
	 * @return double 
	 */
	double getSizeBeforeAtSameLevel(LOrder *order) override;

	// implements AccountListener
	/**
	 * @brief Triggered callback on set position,able to trigger publishRisk
	 * 
	 * @param acct 
	 * @param cid 
	 * @param oldPos oldpositions
	 * @param pos  positions
	 */
	void onPositionSet(Account *acct, long cid, double oldPos, double pos) override;
	/**
	 * @brief Triggered callback on risk update,able to trigger publishRisk
	 * 
	 * @param acct 
	 * @param riskId 
	 * @param oldRisk 
	 * @param risk 
	 * @param localRiskChanged 
	 */
	void onRiskUpdate(Account *acct, int riskId, double oldRisk, double risk, bool localRiskChanged) override;

	// time
	/**
	 * @brief Returns the current time (live)or Current backtest time(sim)
	 * 
	 * @return nanoseconds 
	 */
	nanoseconds currentTS() { return _isLive ? now() : TimerManager::instance()->currentTime(); }

private:
	/**
	 * @brief start-stop trading
	 * 
	 * @param enable 
	 */
	void enableTrading(bool enable);
	/**
	 * @brief pass the message to strategy
	 * 
	 * @param ntype 
	 */
	void notify(NotificationTypes ntype = NT_BOOK);
	/**
	 * @brief pass the message to strategy
	 * 
	 * @param ntype 
	 * @param eventTime 
	 */
	void notify(NotificationTypes ntype, nanoseconds &eventTime);
	// void preprocessEvent(nanoseconds& eventTime);

	// market
	/**
	 * @brief market event callback
	 * 
	 */
	void onMarketEvent();
	/**
	 * @brief Processing after receiving transaction information,update SymbolInfo...
	 * 
	 * @param cid 
	 * @param trade 
	 */
	void processTradeMessage(int cid, TradeUpdate *trade);
	/**
	 * @brief Processing after receiving depth information,update SymbolInfo...
	 * 
	 * @param cid 
	 * @param bh 
	 */
	void processDepthMessage(int cid, DepthUpdate *bh);
#ifndef NO_PB
	/**
	 * @brief Processing after receiving cob information
	 * 
	 * @param cid 
	 * @param data 
	 */
	void processCOBMessage(int cid, CTradeOrderData *data);
#endif

	// Trade
	/**
	 * @brief Processing after receiving balance information
	 * 
	 * @param balance 
	 */
	void processBalanceMessage(CBalanceData *balance);
	/**
	 * @brief Processing after receiving position information
	 * 
	 * @param position 
	 */
	void processPositionMessage(CPositionData *position);
	
	/**
	 * @brief Process volatility message
	 * 
	 * @param volatility 
	 */
	void processVolatilityMessage(CVolatilityData *volatility);
	/**
	 * @brief Processing after receiving order response infomation
	 * 
	 * @param msg 
	 */
	void processOrderMessage(MarketMessage *msg);

	// redis
	/**
	 * @brief subscribe redis
	 * 
	 */
	void subscribeRedis();
	/**
	 * @brief subscribe redis marketdata
	 * 
	 * @param channels 
	 */
	void subscribeRedisMarketData(set<string> &channels);
	/**
	 * @brief subscribe redis riskdata
	 * 
	 * @param channels 
	 * @param channelsToQuery 
	 */
	void subscribeRedisRisks(set<string> &channels, set<string> &channelsToQuery);
	/**
	 * @brief subscribe redis userdata
	 * 
	 * @param channels 
	 * @param channelsToQuery 
	 */
	void subscribeRedisUserData(set<string> &channels, set<string> &channelsToQuery);
	/**
	 * @brief trigger here callback when balance changed,update BalanceItem
	 * 
	 * @param itemName item name
	 * @param balance old balance 
	 */
	void onBalanceChange(string itemName, double balance);
	/**
	 * @brief trigger here callback when balance changed,update BalanceItem
	 * 
	 * @param msg new balance object
	 */
	void onBalanceChange(json &msg);
#ifndef NO_PB
	/**
	 * @brief trigger here callback when balance changed,update BalanceItem
	 * 
	 * @param msg 
	 */
	void onBalanceChange(const Apifiny::Balance &msg);
#endif
	/**
	 * @brief trigger here callback when position changed,update ContractPositionsItem
	 * 
	 * @param msg 
	 */
	void onContractPositionsChange(json &msg);
	/**
	 * @brief trigger here callback when position changed,update ContractPositionsItem
	 * 
	 * @param msg 
	 */
	void onContractPositionsChange(const Apifiny::ContractPosition &msg);
	/**
	 * @brief trigger here callback when risk updated,update accountRisk
	 * 
	 * @param msg 
	 */
	void onRiskReceived(json &msg);
	/**
	 * @brief trigger here callback when recived cmd,handle cmd
	 * 
	 * @param msg 
	 */
	void onCommand(json &msg);
	/**
	 * @brief handle message from redis,such as depth,l1book,balance,risk,useryinfo...
	 * 
	 * @param msg 
	 */
	void processRedisMesssage(json &msg);
	/**
	 * @brief trigger here callback when received redis message,hand it over to processRedisMesssage
	 * 
	 * @param c 
	 * @param reply 
	 * @param privdata 
	 */
	static void onRedisGetMessage(redisAsyncContext *c, void *reply, void *privdata);
	/**
	 * @brief trigger here callback when received subjected message,hand it over to processRedisMesssage
	 * 
	 * @param c 
	 * @param reply 
	 * @param privdata 
	 */
	static void onRedisSubMessage(redisAsyncContext *c, void *reply, void *privdata);

	// redis pub (heartbeat, model_stats...)
	/**
	 * @brief Initialize redis in a thread
	 * 
	 */
	void initRedisPub();
	/**
	 * @brief handle publish messages received from redis
	 * 
	 */
	void redisPubRun();
	/**
	 * @brief implementation to publish cmd
	 * 
	 * @param msmsg CmdRespMessage
	 */
	void publishCmdRespImpl(CmdRespMessage *msmsg);
	/**
	 * @brief implementation to publish the heartbeat
	 * 
	 * @param hbmsg HeartbeatMessage
	 */
	void publishHeartBeatImpl(HeartbeatMessage *hbmsg);
	/**
	 * @brief implementation to publish the model status
	 * 
	 * @param msmsg ModelStatsMessage
	 */
	void publishModelStatsImpl(ModelStatsMessage *msmsg);
	/**
	 * @brief implementation to publish the app status
	 * 
	 * @param msg AppStatsMessage
	 */
	void publishAppStatsImpl(AppStatsMessage *msg);
	/**
	 * @brief structure AppStatsMessage and put in _redisPubMsgQueue
	 * 
	 */
	void publishAppStats();
	/**
	 * @brief implementation to  publish the risk
	 * 
	 * @param msg RiskPubMessage
	 */
	void publishRiskImpl(RiskPubMessage *msg);
	/**
	 * @brief structure RiskPubMessage and put in _redisPubMsgQueue
	 * 
	 * @param account 
	 */
	void publishRisk(Account *account);

	// commands
	void handleMyCommand(const string &action, json &cmd);
	/**
	 * @brief structure CmdRespMessage include risk info and put in _redisPubMsgQueue
	 * 
	 */
	void showRisks();

	// event time log
	/**
	 * @brief initial a thread to log eventtime
	 * 
	 */
	void initEventTimeLog();
	/**
	 * @brief a loop to record log
	 * 
	 */
	void eventTimeLogRun();
	/**
	 * @brief implementation to record event time log
	 * 
	 * @param msg 
	 */
	void logEventTimeImpl(EventTimeMessage *msg);
	/**
	 * @brief structure EventTimeMessage include trade info and put in _eventTimeMsgQueue
	 * 
	 * @param msg 
	 */
	void logTradeEventTime(MarketMessage *msg);
	/**
	 * @brief structure EventTimeMessage include depth info and put in _eventTimeMsgQueue
	 * 
	 * @param msg 
	 */
	void logDepthEventTime(MarketMessage *msg);
	/**
	 * @brief structure EventTimeMessage include notice info and put in _eventTimeMsgQueue
	 * 
	 */
	void logEventNoticeTime();

	// order
	/**
	 * @brief Triggered callback by an create or cancel order response,handle to OrderEventImpl
	 * 
	 */
	void onOrderEvent();
	/**
	 * @brief implementation to handle the order response event to processOrderCreateResp or processOrderCancelResp
	 * 
	 * @param item 
	 */
	void OrderEventImpl(OrderResp *item);
	/**
	 * @brief process the response of order create,such as record log ,update CachedOrder...
	 * 
	 * @param resp 
	 */
	void processOrderCreateResp(OrderCreateResp *resp);
	/**
	 * @brief process the response of order cancel,such as record log ,update CachedOrder...
	 * 
	 * @param resp 
	 */
	void processOrderCancelResp(OrderCancelResp *resp);
	/**
	 * @brief Triggered callback on order created
	 * 
	 * @param order 
	 */
	void onOrderCreated(LOrder *order);
	/**
	 * @brief Triggered callback on order sent
	 * 
	 * @param order 
	 */
	void onOrderSent(LOrder *order);
	/**
	 * @brief Triggered callback on order rejected
	 * 
	 * @param order 
	 */
	void onOrderRejected(LOrder *order);
	/**
	 * @brief Triggered callback on cancelorder created
	 * 
	 * @param order 
	 */
	void onOrderCancelCreated(LOrder *order);
	/**
	 * @brief Triggered callback on cancelorder sent
	 * 
	 * @param order 
	 */
	void onOrderCancelSent(LOrder *order);
	/**
	 * @brief Triggered callback on cancelorder rejected
	 * 
	 * @param order 
	 */
	void onOrderCancelRejected(LOrder *order);
	/**
	 * @brief Triggered callback on order filled
	 * 
	 * @param order 
	 * @param price 
	 * @param size 
	 * @param liq 
	 */
	void onOrderFilled(LOrder *order, double price, double size, Liquidity liq);
	/**
	 * @brief Triggered callback on order canceled
	 * 
	 * @param order 
	 */
	void onOrderCanceled(LOrder *order);
	/**
	 * @brief Triggered callback on order closed
	 * 
	 * @param order 
	 */
	void onOrderClosed(LOrder *order);
	/**
	 * @brief Connect directly to the exchange to obtain market information
	 * 
	 */
	void initDirectMarket();
	/**
	 * @brief Directly connected to exchanges for trading
	 * 
	 */
	void initDirectTrader();
	/**
	 * @brief Connect to COB platform for trading
	 * 
	 */
	void initPlatformTrader();
	/**
	 * @brief Connect to COB platform to obtain market information
	 * 
	 */
	void initPlatformMarketdata();

	void initExchanges();

	// HUOBI order sepcific
	/**
	 * @brief initialize huobi api 
	 * 
	 * @param cfg 
	 */
	void initHuobi(json &cfg);
	/**
	 * @brief process huobi order update
	 * 
	 * @param data 
	 */
	void processHuobiOrderUpdate(json &data);
	/**
	 * @brief process huobi order update
	 * 
	 * @param msg MarketMessage
	 */
	void processHuobiOrderUpdate(MarketMessage *msg);
	/**
	 * @brief update huobiUnknowOrderUpdateCache,If it matches the remoteOrderId, it is distributed to processHuobiOrderUpdate for processing
	 * 
	 * @param remoteOrderId 
	 */
	void processHuobiCachedOrderUpdates(long remoteOrderId);

	//HUOBI order sepcific
	/**
	 * @brief initial huobi swap api
	 * 
	 * @param cfg 
	 */
	void initHuobiSwap(json& cfg);
	// void processHuobiSwapOrderUpdate(json& data);
	/**
	 * @brief process huobi swap order update,update _huobiSwapOrderMap
	 * 
	 * @param msg 
	 */
	void processHuobiSwapOrderUpdate(MarketMessage* msg);
	/**
	 * @brief update _huobiSwapUnknowOrderUpdateCache,If it matches the remoteOrderId, it is distributed to processHuobiSwapOrderUpdate for processing
	 * 
	 * @param remoteOrderId 
	 */
	void processHuobiSwapCachedOrderUpdates(long remoteOrderId);

	// BINANCE order sepcific
	/**
	 * @brief initial binance ecg api
	 * 
	 * @param cfg 
	 */
	void initBinance(json &cfg);

	/**
	 * @brief process binance order update ,update _binanceOrderMap
	 * 
	 * @param data 
	 */
	void processBinanceOrderUpdate(json &data);
	/**
	 * @brief process binance order update ,update _binanceOrderMap
	 * 
	 * @param msg 
	 */
	void processBinanceOrderUpdate(MarketMessage *msg);
	/**
	 * @brief update _binanceDataUnknownUpdateCache,If it matches the remoteOrderId, it is distributed to processBinanceOrderUpdate for processing
	 * 
	 * @param remoteOrderId 
	 */
	void processBinanceCachedOrderUpdates(long remoteOrderId);

	// Binance swap
	/**
	 * @brief init binance swap api
	 * 
	 * @param cfg 
	 */
	void initBinanceSwap(json &cfg);
	/**
	 * @brief process binance swap order update,update _binanceSwapOrderMap
	 * 
	 * @param data 
	 */
	void processBinanceSwapOrderUpdate(json &data);
	/**
	 * @brief process binance swap order update,update _binanceSwapOrderMap
	 * 
	 * @param msg 
	 */
	void processBinanceSwapOrderUpdate(MarketMessage *msg);
	/**
	 * @brief update _binanceSwapDataUnknownUpdateCache,If it matches the remoteOrderId, it is distributed to processBinanceSwapOrderUpdate for processing
	 * 
	 * @param remoteOrderId 
	 */
	void processBinanceSwapCachedOrderUpdates(long remoteOrderId);

	// Binance us
	/**
	 * @brief initialize binanceus ecg api
	 * 
	 * @param cfg 
	 */
	void initBinanceUs(json &cfg);
	/**
	 * @brief process binanceus order update,update _binanceUsMarginOrderMap or _binanceUsOrderMap
	 * 
	 * @param data 
	 */
	void processBinanceUsOrderUpdate(json &data);
	/**
	 * @brief process binanceus order update,update _binanceUsMarginOrderMap or _binanceUsOrderMap
	 * 
	 * @param msg 
	 */
	void processBinanceUsOrderUpdate(MarketMessage *msg);
	/**
	 * @brief update _binanceUsDataUnknownUpdateCache,If it matches the remoteOrderId, it is distributed to processBinanceUsOrderUpdate for processing
	 * 
	 * @param remoteOrderId 
	 */
	void processBinanceUsCachedOrderUpdates(long remoteOrderId);

	// OKCOIN spot order sepcific
	/**
	 * @brief initial okcoin ecg api
	 * 
	 * @param cfg 
	 */
	void initOkcoin(json &cfg);
	/**
	 * @brief process okcoin order update,update _okcoinOrderMap
	 * 
	 * @param data 
	 */
	void processOkcoinOrderUpdate(json &data);
	/**
	 * @brief process okcoin order update,update _okcoinOrderMap
	 * 
	 * @param msg 
	 */
	void processOkcoinOrderUpdate(MarketMessage *msg);
	/**
	 * @brief update _okcoinUnknowOrderUpdateCache,If it matches the remoteOrderId, it is distributed to processOkcoinOrderUpdate for processing
	 * 
	 * @param remoteOrderId 
	 */
	void processOkcoinCachedOrderUpdates(long remoteOrderId);

	// FTX spot
	/**
	 * @brief initial ftx ecg api
	 * 
	 * @param cfg 
	 */
	void initFtx(json &cfg);
	/**
	 * @brief process ftx order update,update _ftxOrderMap
	 * 
	 * @param msg 
	 */
	void processFtxOrderUpdate(MarketMessage *msg);
	/**
	 * @brief update _ftxUnknowOrderUpdateCache,If it matches the remoteOrderId, it is distributed to processFtxOrderUpdate for processing
	 * 
	 * @param remoteOrderId 
	 */
	void processFtxCachedOrderUpdates(long remoteOrderId);

	// FTX swap
	/**
	 * @brief initial ftx swap api
	 * 
	 * @param cfg 
	 */
	void initFtxSwap(json &cfg);
	/**
	 * @brief process ftx swap order update,update _ftxSwapOrderMap
	 * 
	 * @param msg 
	 */
	void processFtxSwapOrderUpdate(MarketMessage *msg);
	/**
	 * @brief update _ftxSwapUnknowOrderUpdateCache,If it matches the remoteOrderId, it is distributed to processFtxSwapOrderUpdate for processing
	 * 
	 * @param remoteOrderId  
	 */
	void processFtxSwapCachedOrderUpdates(long remoteOrderId);

	// OKEX spot order sepcific
	/**
	 * @brief initial okex ecg api
	 * 
	 * @param cfg 
	 */
	void initOkex(json &cfg);
	/**
	 * @brief process okex order update,update _okexOrderMap
	 * 
	 * @param data 
	 */
	void processOkexOrderUpdate(json &data);
	/**
	 * @brief process okex order update,update _okexOrderMap
	 * 
	 * @param msg 
	 */
	void processOkexOrderUpdate(MarketMessage *msg);
	/**
	 * @brief update _okexUnknowOrderUpdateCache,If it matches the remoteOrderId, it is distributed to processOkexOrderUpdate for processing
	 * 
	 * @param remoteOrderId 
	 */
	void processOkexCachedOrderUpdates(long remoteOrderId);

	// OKEX spot swap order sepcific
	/**
	 * @brief initial okex swap api
	 * 
	 * @param cfg 
	 */
	void initOkexSwap(json &cfg);
	/**
	 * @brief process okex swap order update,update _okexOrderMap
	 * 
	 * @param data 
	 */
	void processOkexSwapOrderUpdate(json &data);
	/**
	 * @brief process okex swap order update,update _okexOrderMap
	 * 
	 * 
	 * @param msg 
	 */
	void processOkexSwapOrderUpdate(MarketMessage *msg);
	/**
	 * @brief update _okexUnknowSwapOrderUpdateCache,If it matches the remoteOrderId, it is distributed to processOkexSwapOrderUpdate for processing
	 * 
	 * @param remoteOrderId 
	 */
	void processOkexSwapCachedOrderUpdates(long remoteOrderId);

	// OKEX futures order sepcific
	/**
	 * @brief init okex futures api
	 * 
	 * @param cfg 
	 */
	void initOkexFutures(json &cfg);
	/**
	 * @brief process okex future order update,update _okexFuturesOrderMap
	 * 
	 * @param data 
	 */
	void processOkexFuturesOrderUpdate(json &data);
	/**
	 * @brief process okex future order update,update _okexFuturesOrderMap
	 * 
	 * @param msg 
	 */
	void processOkexFuturesOrderUpdate(MarketMessage *msg);
	/**
	 * @brief update _okexFuturesUnknownUpdateCache,If it matches the remoteOrderId, it is distributed to processOkexFuturesOrderUpdate for processing
	 * 
	 * @param remoteOrderId 
	 */
	void processOkexFuturesCachedOrderUpdates(long remoteOrderId);

	// COB futures order specific
	/**
	 * @brief init cob virtul ecg api
	 * 
	 * @param cfg 
	 * @param exchange 
	 */
	void initCOB(json &cfg, std::string exchange);
	/**
	 * @brief process create order confirm of cob,update _cobOrderMap
	 * 
	 * @param data 
	 */
	void processCOBOrderCreateAck(Apifiny::TradeRespMsg *data);
	/**
	 * @brief process cancel order confirm of cob,update _cobOrderMap
	 * 
	 * @param data 
	 */
	void processCOBOrderCancleAck(Apifiny::TradeRespMsg *data);
	/**
	 * @brief process  order update of cob,update _cobOrderMap
	 * 
	 * @param data 
	 */
	void processCOBOrderUpdate(Apifiny::TradeRespMsg *data);
	/**
	 * @brief update _cobUnknowUpdateCache,If it matches the remoteOrderId, it is distributed to processCOBOrderUpdate for processing
	 * 
	 * @param remoteOrderId 
	 */
	void processCOBCachedOrderUpdates(string remoteOrderId);
	void processCOBTradeUpdate(Apifiny::TradeRespMsg *data);
	/**
	 * @brief process balance update of cob,update BalanceItem
	 * 
	 * @param data 
	 */
	void processCOBBalanceUpdate(Apifiny::TradeRespMsg *data);
	/**
	 * @brief process contract position update of cob,update ContractPositionsItem
	 * 
	 * @param data 
	 */
	void processCOBContractPositionUpdate(Apifiny::TradeRespMsg *data);
	/**
	 * @brief process order update of cob,update BalanceItem
	 * 
	 * @param data 
	 * @param order 
	 */
	void processCOBOrderUpdate(Apifiny::TradeRespMsg *data, LOrder *order);

	// warmup using recorded market data
	void warm_up_init();
	void warm_up_run();
	void loadMessages(std::chrono::nanoseconds nextTS);
	Message *getNextMessage();
	void processTrade2Message(Message *msg);
	bool processQuote3Message(Message *msg);
	/**
	 * @brief process raw data such marketmessage into platform
	 * 
	 * @param msg 
	 * @param notType 
	 * @return msgNeedNotify 
	 */
	bool processCcRawMessage(Message *msg, NotificationTypes &notType);

	// CCBaseTrader
	/**
	 * @brief  timed event,notify NT_TIMER
	 * 
	 */
	void onTimerEvent();
	/**
	 * @brief Triggered callback by book change,distrabute to processDepthMessage to process
	 * 
	 * @param msg 
	 */
	void onCCJsonBookChange(json &msg) override;
	/**
	 * @brief Triggered callback by trades, distrabute to processTradeMessage to process
	 * 
	 * @param msg 
	 */
	void onCCJsonTrade(json &msg) override;
	/**
	 * @brief Triggered callback by userdata,distrabute to processOrderUpdate to process
	 * 
	 * @param msg 
	 */
	void onUserData(json &msg);

public:
	OKEX_V5::OkexSpotTradeHandler *_okexSpotTradeHandler = nullptr;
	OKEX_V5::OkexSwapTradeHandler *_okexSwapTradeHandler = nullptr;

private:
	NotificationManager *_notMgr;
	TimerManager *_timerMgr;
	SymbolManager *_symMgr;
	AccountManager *_acctMgr;
	StrategyManager *_strategyMgr;
	BalanceManager *_balanceMgr;
	VolatilityManager* _volatilityMgr;
	ContractPositionsManager *_cposMgr;
	CobSessionHandle *_cobsession = nullptr;
	TraderCob *_traderCob = nullptr;

	string _redisServerAddr = "127.0.0.1";

	// common
	bool _tradingEnabled = false;
	long _lastNoticeId = 0;
	/**
	 * @brief config json
	 * 
	 */
	json _cfg;
	/**
	 * @brief live mode or sim mode
	 * 
	 */
	bool _isLive = false;
	bool _isRedisPub = true;
	int _tradeDate;
	string _appInstName;
	long _riskInstId;
	bool _warmupCompleted = false;
	bool _hasTradeNotify = false;
	bool _hasBookNotify = false;
	/**
	 * @brief Minimum notional value to trigger notifications
	 * 
	 */
	double _minNotifiableNotional = 0.0;

	// events
	struct event_base *_eventBase;
	struct event *_timerEvent;
	struct event *_orderEvent;
	struct event *_marketEvent;

	// redis pub (heartbeat, model_stats ...)
	redisContext *_redisPubCtxt = NULL;
	/**
	 * @brief the queue to cache received redis publish message
	 * 
	 */
	RedisPubMessageBQueue _redisPubMsgQueue;

	// event time log
	std::ofstream *_eventTimeOS;
	EventTimeMessageBQueue _eventTimeMsgQueue;

	// market
	string _marketDataType = "DIRECT";
	/**
	 * @brief the queue to cache received market message
	 * 
	 */
	MarketMessageQueue _marketMsgQueue;
	BitmexMarketHandler *_bitmexMarketHandler = nullptr;
	HuobiTradeHandler *_huobiTradeHandler = nullptr;
	// HuobiMarketHandler* _huobiMarketHandler = nullptr;
	HUOBI_V2::HuobiMarketHandler *_huobiMarketHandler = nullptr;
	HuobiSwapTradeHandler* _huobiSwapTradeHandler = nullptr;
	HuobiSwapMarketHandler* _huobiSwapMarketHandler = nullptr;
	BinanceSpotMarketHandler *_binanceMarketHandler = nullptr;
	BinanceSpotTradeHandler *_binanceSpotTradeHandler = nullptr;
	BinanceSpotTradeHandler *_binanceMarginTradeHandler = nullptr;
	BinanceSwapMarketHandler *_binanceSwapMarketHandler = nullptr;
	BinanceSwapTradeHandler *_binanceSwapTradeHandler = nullptr;
	BinanceUsMarketHandler *_binanceUsMarketHandler = nullptr;
	BinanceUsTradeHandler *_binanceUsTradeHandler = nullptr;
	OKCOIN::OkcoinSpotMarketHandler *_okcoinSpotMarketHandler = nullptr;
	OKCOIN::OkcoinSpotTradeHandler *_okcoinSpotTradeHandler = nullptr;
	OKEX_V5::OkexSpotMarketHandler *_okexSpotMarketHandler = nullptr;
	OKEX_V5::OkexSwapMarketHandler *_okexSwapMarketHandler = nullptr;
	OKEX_V5::OkexFuturesMarketHandler *_okexFuturesMarketHandler = nullptr;
	OKEX_V5::OkexFuturesTradeHandler *_okexFuturesTradeHandler = nullptr;
	FTX::FtxSpotMarketHandler *_ftxSpotMarketHandler = nullptr;
	FTX::FtxSpotTradeHandler *_ftxSpotTraderHandler = nullptr;
	FTX::FtxSwapMarketHandler *_ftxSwapMarketHandler = nullptr;
	FTX::FtxSwapTradeHandler *_ftxSwapTraderHandler = nullptr;

	IBL2MarketHandler *_ibl2MarketHandler = nullptr;
	CobMarketHandler *_cobMarketHandler = nullptr;

	// order processing
	long _localOrderId = 1;
	/**
	 * @brief the map cached opening orders
	 * 
	 */
	LOrderMap _orderMap;
	/**
	 * @brief the queue to cache received order response message
	 * 
	 */
	OrderRespQueue _orderRespQueue;
	/**
	 * @brief the map cached trades info
	 * 
	 */
	SymbolTraderMap _symbolTraderMap;

	// binance
	LOrderMap _binanceOrderMap;
	LOrderMap _binanceMarginOrderMap;
	JsonListMap _binanceUnknownUpdateCache;
	LOrderDataMap _binanceDataUnknownUpdateCache;

	// binance swap
	LOrderMap _binanceSwapOrderMap;
	JsonListMap _binanceSwapUnknownUpdateCache;
	LOrderDataMap _binanceSwapDataUnknownUpdateCache;

	// binance us
	LOrderMap _binanceUsOrderMap;
	LOrderMap _binanceUsMarginOrderMap;
	// JsonListMap _binanceUsUnknownUpdateCache;
	LOrderDataMap _binanceUsDataUnknownUpdateCache;

	// huobi
	LOrderMap _huobiOrderMap;
	LOrderDataMap _huobiUnknowOrderUpdateCache;
	JsonListMap _huobiUnknownUpdateCache;

	//huobi swap
	LOrderMap _huobiSwapOrderMap;
	LOrderDataMap _huobiSwapUnknowOrderUpdateCache;
	JsonListMap _huobiSwapUnknownUpdateCache;

	// okcoin
	LOrderMap _okcoinOrderMap;
	LOrderDataMap _okcoinUnknowOrderUpdateCache;
	JsonListMap _okcoinUnknownUpdateCache;

	// ftx spot
	LOrderMap _ftxOrderMap;
	LOrderDataMap _ftxUnknowOrderUpdateCache;
	JsonListMap _ftxUnknownUpdateCache;

	// ftx swap
	LOrderMap _ftxSwapOrderMap;
	LOrderDataMap _ftxSwapUnknowOrderUpdateCache;
	JsonListMap _ftxSwapUnknownUpdateCache;

	// okex
	LOrderMap _okexOrderMap;
	LOrderDataMap _okexUnknowOrderUpdateCache;
	JsonListMap _okexUnknownUpdateCache;

	// okex futures
	LOrderMap _okexFuturesOrderMap;
	JsonListMap _okexFuturesUnknownUpdateCache;

	// okex swap
	LOrderMap _okexSwapOrderMap;
	LOrderDataMap _okexUnknowSwapOrderUpdateCache;
	JsonListMap _okexSwapUnknownUpdateCache;

	EXCHANGE_DEFINE(CoinbaseSpot)
	EXCHANGE_DEFINE(DeribitSwap)
	EXCHANGE_DEFINE(DeribitOption)


	// cob
	LCobOrderMap _cobOrderMap;
	TradeRespListMap _cobUnknowUpdateCache;

	// performance measuement
	nanoseconds _lastMarketEventTime;
	nanoseconds _lastNoticeTime;

	// warmup
	bool _warmupSystem = false;
	PlayerManager *_playerMgr;
	typedef unordered_map<int, FeedParser *> FeedParserMap;
	MessageQueue _msgQueue;
	std::chrono::nanoseconds _next_timeout;
	std::chrono::nanoseconds _warmup_start_time;
	FeedParserMap _feedParserMap;

	// sim
	SimMarket *_simMarket = nullptr;
	nanoseconds _simStartTime;
	nanoseconds _simEndTime;
};
