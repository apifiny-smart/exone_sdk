#pragma once
#include "sim/SimEngine.h"
#include "components/PrinterManager.h"
#include "components/StrategyManager.h"

class SimTrader: public SimEngine{
public:
	SimTrader(json& cfg);
	~SimTrader();

private:
	void notify(long id);
	void enableTrading(bool enable);
	void loadDynlibs();

private:
	StrategyManager* _strategyMgr;
	PrinterManager* _printerMgr;

	bool _hasStrategy;
	bool _hasPrinter;
};

