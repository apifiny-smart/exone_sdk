#include "CCTrader.h"
#include "base/XlibComponentFactory.h"
#include "SimpleTakeStrategy.h"

class MyComponentFactory : public XlibComponentFactory
{
public:
	Strategy *getStrategy(const string &type, const string &name, const json &attr)
	{
		if (type == "SimpleTakeStrategy") 
			return new SimpleTakeStrategy(name, attr);            
		return nullptr;
	}	
};

int main(int argc, char **argv)
{
	if (argc <= 1)
	{
		std::cout << "Usage: ccc_simple_taker cfg_path" << std::endl;
		return 0;
	}

	//load configuration
	std::time_t t = std::time(0);
	std::tm *now = std::localtime(&t);
	int date = (now->tm_year + 1900) * 10000 + (now->tm_mon + 1) * 100 + now->tm_mday;
	json cfg = loadConfig(argv[1],date);
	json &instCfg = cfg["instance"];
	instCfg["tradeDate"] = date;
	instCfg["isLive"] = true;

	//setup logger
	string instName = getJsonValue(instCfg, "name", string("instname"));
	string logPath = getJsonValue(instCfg, "log_path", string("."));
	date = instCfg["tradeDate"];
	logPath += "/" + to_string(date);
	mkdirs(logPath);
	logPath += "/inst_" + instName + ".log";
	int logLevel = getJsonValue(instCfg, "log_level", qts::log4z::LOG_LEVEL_DEBUG);
	QTS_LOG_START(logLevel, logPath);

	//setup your custom component factory to load components
	XlibComponentFactoryPtr xmgr(new MyComponentFactory);
	StrategyManager::instance()->addXlibComponentFactory(xmgr);
	
	//start cctrader
	std::cout << "Starting trader ..." << std::endl;
	CCTrader client(cfg);
	client.initialize();
	client.run();
}
