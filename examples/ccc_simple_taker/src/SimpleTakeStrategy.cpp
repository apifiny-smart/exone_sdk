#include "SimpleTakeStrategy.h"

void SimpleTakeStrategy::init() {
	auto symMgr = SymbolManager::instance();

	string symbol = _cfg.at("symbol");
	string exch = _cfg.at("trade_market");
	auto sh = symMgr->getSymbolHandler(symbol, exch);
    sh->addListener(this);
	_si = &sh->symbolInfo();

	_timerMgr = TimerManager::instance();
	TimerHandler* th = _timerMgr->getTimerHandler(60000);
	th->addListener(this);

	_take_from_mid = _cfg.at("take_from_mid_bps").get<double>() * 0.0001;
	_order_size = _cfg.at("order_size");
}

void SimpleTakeStrategy::onNotify() {
    if(!_si->is_ready()) 
		return;
	
	if(_hasTimeEvt) {
		_hasTimeEvt = false;

		std::cout << "send a random IOC order" << std::endl;

		LOrder* ord = new LOrder();
		ord->sender = this;
		ord->account = 101;
		ord->use_margin = false;
		ord->margin_source = "spot";
		ord->side = BUY;
		ord->si = _si;
		ord->px = _si->mid_px() * (1 + _take_from_mid);
		ord->qty = _order_size;
		ord->remainingQty = ord->qty;
		ord->signal = 0.0;
		ord->spread = _si->spread();
		ord->tif = Tif::TIF_IOC;
		ord->intention = OrderIntention::OI_AGGRESSIVE;

	    TradeApi* tradeApi = TradeApiManager::instance()->tradeApi();
		tradeApi->sendOrder(ord);
    }
}

void SimpleTakeStrategy::onQuote(long cid, const SymbolInfo& si) {

}

void SimpleTakeStrategy::onTick(long cid, const SymbolInfo& si) {

}

void SimpleTakeStrategy::onTimer(TimerHandler* th, long msecs) {
	_hasTimeEvt = true;
}