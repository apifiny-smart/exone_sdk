#!lua

-- solution
solution "exone_examples"
   configurations { "Debug", "Release" }

--projects
project "alpha01"
  kind "SharedLib"
  language "C++"
  includedirs { 
    "../../../include",    
    "../src"
  }
  files { 
    "../src/**.cpp"    
  }
  objdir "../../../build/obj"

  configuration "Debug"
    targetdir "../../../build/bin/debug"
    defines { "DEBUG", "USE_QTS_LOG4","NO_PB" }
    flags { "Symbols" }
    libdirs {       
      "../../../bin"
    }
    links {"exone_trader",  "pthread", "boost_iostreams", "boost_system", "boost_filesystem" }
    buildoptions { "-std=c++17", "-fPIC", "-rdynamic" }
    postbuildcommands { "cp -f ../../../build/bin/debug/libalpha01.so ../../../bin/xlibs" }
    
  configuration "Release"
    targetdir "../../../build/bin/rel"
    defines { "NDEBUG", "USE_QTS_LOG4","NO_PB" }
    flags { "Optimize" }
    libdirs {
      "../../../bin"
    }
    links {"exone_trader", "pthread", "boost_iostreams", "boost_system", "boost_filesystem" }
    buildoptions { "-std=c++17", "-fPIC", "-rdynamic" }
    postbuildcommands { "cp -f ../../../build/bin/rel/libalpha01.so ../../../bin/xlibs" }    
