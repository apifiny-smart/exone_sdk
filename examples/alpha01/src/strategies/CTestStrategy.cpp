#include "CTestStrategy.h"
// #include "components/VariableManager.h"
#include "components/SymbolManager.h"
#include "common/common.h"
#include <limits>
#include <cmath>

/////////////////////// CTestStrategy ///////////////////////////

//测试 下单 价格 方向 回报 balance account notion pnl volume


void CTestStrategy::init()
{
		// setup logger
	// std::time_t t = std::time(0);
	// std::tm *now = std::localtime(&t);
	// int date = (now->tm_year + 1900) * 10000 + (now->tm_mon + 1) * 100 + now->tm_mday;

	
	// string logPath = "./" + to_string(date);
	// mkdirs(logPath);
	// logPath += "/inst_CTestStrategy.log";
	// int logLevel = qts::log4z::LOG_LEVEL_DEBUG;
	// QTS_LOG_START(logLevel, logPath);
	// QTS_LOG_A("id_log:%s","222");
	QTS_LOG_D("id_log:%s","222");
// 	#define QTS_LOG_A LOG_A
// #define QTS_LOG_E LOG_E
// #define QTS_LOG_W LOG_W
// #define QTS_LOG_I LOG_I
// #define QTS_LOG_D LOG_D
// #define QTS_LOG_T LOG_T

	// Strategy::init();

	SymbolManager *symMgr = SymbolManager::instance();
	// PricingModelManager *pmMgr = PricingModelManager::instance();
	_notMgr = NotificationManager::instance();

	_symbol = _cfg.at("symbol");
	_trade_market = _cfg.at("trade_market");

	auto tradeApiMgr = TradeApiManager::instance();
	_tradeApi = tradeApiMgr->tradeApi();

	_symMgr = SymbolManager::instance();
	_balanceMgr = BalanceManager::instance();

	auto sh = _symMgr->getSymbolHandler(_symbol, _trade_market);
	_si = &sh->symbolInfo();
	sh->addListener(this);


	// send order
	// LOrder *ord = new LOrder();
	// LOrder *order = _tradeApi->sendOrder(ord);

	// cancelOrder
	// LOrder *ord = new LOrder();
	// _tradeApi->cancelOrder(ord);
	
	std::cout << "created CTestStrategy:" << name() << std::endl;
}

void CTestStrategy::notify(long id, bool force)
{
	std::cout << "CTestStrategy notify " << id << " " << force << std::endl;
	Strategy::notify(id, force);
	// QTS_LOG_A("id_log:%s",doubleToString(id).c_str());
	//
	if (_tested && _si->is_ready())
	{
		_tested = false;
		//获取对手价格
		// cout << "对手价格:"
		//  << _si->getBestPrice(BUY)
		//  << endl;
		// SLOG << " - update balance " << itemName << " = " << bi->balance << endl;
		// QTS_LOG_A("对手价格:%s,%ld,%s,%s,%ld", toStr(eventTime).c_str(), eventTime.count(), order->si->ticker.c_str(), order->si->exchange.c_str(), order->orderId);
		QTS_LOG_D("对手价格log:%s",doubleToString(_si->getBestPrice(BUY)).c_str());

		

		//测试 下单 状态流转 成交价格 成交量 notional
		LOrder *ord = new LOrder();
		ord->sender = this;
		ord->account = 0;
		ord->use_margin = false;
		ord->side = BUY;
		ord->si = _si;
		ord->px = _si->getBestPrice(BUY)+1.0;
		ord->qty = 0.0001;
		ord->remainingQty = ord->qty;
		ord->signal = 0;
		ord->spread = _si->spread();
		ord->tif = Tif::TIF_IOC;
		ord->intention = OrderIntention::OI_AGGRESSIVE;
		ord->noticeId = _notMgr->getNoticeId();

		LOrder *order = _tradeApi->sendOrder(ord);
		printOrderStatus(order,"IOC下单");

		cout << "异常价格下单:"
		 << _si->getBestPrice(SELL)
		 << endl;
		LOrder *ord_err = new LOrder();
		ord_err->sender = this;
		ord_err->account = 0;
		ord_err->use_margin = false;
		ord_err->side = SELL;
		ord_err->si = _si;
		ord_err->px = _si->getBestPrice(SELL)+2.0;
		ord_err->qty = 0.0001;
		ord_err->remainingQty = ord_err->qty;
		ord_err->signal = 0;
		ord_err->spread = _si->spread();
		ord_err->tif = Tif::TIF_IOC;
		ord_err->intention = OrderIntention::OI_AGGRESSIVE;
		ord_err->noticeId = _notMgr->getNoticeId();

		LOrder *order_err = _tradeApi->sendOrder(ord_err);
		printOrderStatus(order_err,"异常价格下单流程");

		cout << "limit下单，当前对手价格:"
		 << _si->getBestPrice(BUY)
		 << endl;

		//测试 下单 状态流转 成交价格 成交量 notional
		LOrder *ord2 = new LOrder();
		ord2->sender = this;
		ord2->account = 0;
		ord2->use_margin = false;
		ord2->side = SELL;
		ord2->si = _si;
		ord2->px = _si->getBestPrice(SELL)-1.0;
		ord2->qty = 0.00011;
		ord2->remainingQty = ord2->qty;
		ord2->signal = 0;
		ord2->spread = _si->spread();
		ord2->tif = Tif::TIF_IOC;
		ord2->intention = OrderIntention::OI_PASSIVE;
		ord2->noticeId = _notMgr->getNoticeId();

		LOrder *order2 = _tradeApi->sendOrder(ord2);
		printOrderStatus(order2,"limit下单");
	}
}

// void CTestStrategy::tradeOnSignal(double signal){

// if(!_si->is_ready()) {
// 	cout<<"CTestStrategy::tradeOnSignal cancelAllOrders.\n";
// 	cancelAllOrders(CR_SIGNAL_NOT_READY);
// 	return;
// }

// double midPx = _si->mid_px();
// double pos = _account->position(_si->cid);
// double notional = _account->notional(_si->cid);

// double mmHalfSpread = midPx * _quote_spread;
// double shift = 0;// -sign(notional) * mmHalfSpread * min(1.0, max(0.0, fabs(notional) / _max_notional));

// double refPx = _depPm ? _depPm->price() : midPx;
// double fv = refPx * (1.0 + signal) + shift;

// double bidTgtPx = fv - mmHalfSpread;
// double askTgtPx = fv + mmHalfSpread;

// maintainOrders(BUY, askTgtPx);
// maintainOrders(SELL, askTgtPx);
// int ret = placeOnTop(BUY,1);
// std::cout<< toStr(now()) <<" - call placeOnTop ret "<<ret<<std::endl;
// }
// make
// void CTestStrategy::maintainOrders(TradeDirs side, double tgt_px) {
// double qpx = roundPrice(tgt_px, side);
// double oppoPx = _si->getOppositeBestPrice(side);
// if(!priceWorseThan(qpx, oppoPx, side)) {
// 	qpx = worsenPrice(oppoPx, _si->tick_size, side);
// }

// if(hasOrders(side)) {
// 	double opx = getMyTopOrderPrice(side);
// 	if(fabs(qpx / opx - 1.0) > _max_quote_error) {
// 		cancelOrders(side, CR_SIGNAL_LOW, 0, false);
// 		//placeNewOrder(side, qpx);
// 	}
// } else {
// 	placeNewOrder(side, qpx);
// }
// }

// double CTestStrategy::calc_desired_order_qty(double px, TradeDirs side) {

// double pos = _account->position(_si->cid);
// double notional = _account->notional(_si->cid);
// double allowed_notional = side==BUY ? (_max_notional - notional) : (_max_notional + notional);
// double order_size = min(_order_notional, allowed_notional)  / _si->contract_multiplier / px;
// //order_size: 0.176543 _order_notional: 300 allowed_notional: 390 _si->contract_multiplier: 10 px: 169.93
// cout<<"CTestStrategy::calc_desired_order_qty order_size: "<<order_size<<" _order_notional: "<<_order_notional<<" allowed_notional: "<<allowed_notional<<" _si->contract_multiplier: "<<_si->contract_multiplier<<" px: "<<px<<endl;
// return order_size;
// 	return 0.0;
// }

// cid:1002 symbol:BSVUSDTSWAP.S tif:day side:0 px:175.1 qty:171.331 bid:166.65 ask:166.77 tgt_px:175.1
//  bool CTestStrategy::placeNewOrder(TradeDirs side, double tgt_px) {

// const nanoseconds& eventTime = _timerMgr->currentTime();
// double px = tgt_px;
// double qty = 1;//calc_desired_order_qty(px, side);

// if(sendIocOrder(side, px, qty, 0.0)) {
// 	if(_isLive)
// 		cout << toStr(eventTime)
// 			<< " QUOTE cid:" << _si->cid
// 			<< " symbol:" << _si->ticker << "." << _si->exchange
// 			<< " tif:day side:" << side
// 			<< " px:" << px << " qty:" << qty
// 			<< " bid:" << _si->bid_px << " ask:" << _si->ask_px
// 			<< " tgt_px:" << tgt_px << endl;
// 	return true;
// }
// 	return false;
// }
// }	

void CTestStrategy::onQuote(long cid, const SymbolInfo &si){
	std::cout<<"onQuote,bid_px :"<<_si->bid_px <<",ask_px :"<<_si->bid_px<<std::endl;
}	
void CTestStrategy::onTick(long cid, const SymbolInfo &si){
	std::cout<<"onTick,bid_tbbo :"<<_si->bid_tbbo <<",ask_tbbo :"<<_si->ask_tbbo<<std::endl;
}	

void CTestStrategy::onOrderCreated(LOrder* order){
	std::cout << " order created. orderId:" << order->orderId <<",status:" << "ADD"<<std::endl;
	printOrderStatus(order,"onOrderCreated");
}	
void CTestStrategy::onOrderAcked(LOrder* order){
	std::cout << " order acked. orderId:" << order->orderId <<",status:" << "ACK"<<std::endl;
	printOrderStatus(order,"onOrderAcked");

}	
void CTestStrategy::onOrderRejected(LOrder* order){
	std::cout << " order rejected. orderId:" << order->orderId <<",status:" << "RJD"<<std::endl;
	printOrderStatus(order,"onOrderRejected");

}	
void CTestStrategy::onOrderCancelCreated(LOrder* order){
	std::cout << " cancel order created. orderId:" << order->orderId <<",status:" << "CXL"<<std::endl;
	printOrderStatus(order,"onOrderCancelCreated");

}	
void CTestStrategy::onOrderCancelAcked(LOrder* order){
	std::cout << " cancel order acked. orderId:" << order->orderId <<",status:" << "CAK"<<std::endl;
	printOrderStatus(order,"onOrderCancelAcked");
}	
void CTestStrategy::onOrderCancelRejected(LOrder* order){
	std::cout << " cancel order rejected. orderId:" << order->orderId <<",status:" << "CRD"<<std::endl;
	printOrderStatus(order,"onOrderCancelRejected");

}	
void CTestStrategy::onOrderExec(TradeDirs side, double px, double qty, Liquidity liq, LOrder* order){
	std::cout << " order exed. orderId:" << order->orderId 
	<< ",side:" << side
	<< ",px:" << px
	<< ",qty:" << qty<<",status:" << "EXE"<<std::endl;
	printOrderStatus(order,"onOrderExec");

}	
void CTestStrategy::onOrderCanceled(LOrder* order){
	std::cout << "  order canceled. orderId:" << order->orderId <<",status:" << "CXD"<<std::endl;
	printOrderStatus(order,"onOrderCanceled");

}	
void CTestStrategy::onOrderClosed(LOrder* order){
	std::cout << " order closed. orderId:" << order->orderId <<",status:" << "CLD"<<std::endl;
	printOrderStatus(order,"onOrderClosed");

}	
void CTestStrategy::printOrderStatus(LOrder *order,string steps)
{
	cout << steps
		 << ",account:"
		 << order->account
		 << ",side:"
		 << order->side
		 << ",si:"
		 << order->si
		 << ",px:"
		 << order->px
		 << ",qty:"
		 << order->qty
		 << ",filledQty:"
		 << order->filledQty
		 << ",filledNotional:"
		 << order->filledNotional
		 << ",filledPx:"
		 << order->filledPx //for futures
		 << ",remainingQty:"
		 << order->remainingQty
		 << endl;

	// QTS_LOG_D("steps:%s,account:%s",steps.c_str(),to_string(order->account).c_str());
	QTS_LOG_D("steps:%s,oid:%s,account:%s,px:%s,qty:%s,filledQty:%s,filledNotional:%s,filledPx:%s,remainingQty:%s",
	steps.c_str(),
	doubleToString(order->orderId).c_str(),
	to_string(order->account).c_str(),
	doubleToString(order->px).c_str(),
	doubleToString(order->qty).c_str(),
	doubleToString(order->filledQty).c_str(),
	doubleToString(order->filledNotional).c_str(),
	doubleToString(order->filledPx).c_str(),
	doubleToString(order->remainingQty).c_str());
}
