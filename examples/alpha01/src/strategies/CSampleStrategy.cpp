#include "CSampleStrategy.h"
#include "components/SymbolManager.h"
#include <limits>
#include <cmath>

/////////////////////// CSampleStrategy ///////////////////////////
void CSampleStrategy::init()
{
	SymbolManager *symMgr = SymbolManager::instance();
	_notMgr = NotificationManager::instance();

	_symbol = _cfg.at("symbol");
	_trade_market = _cfg.at("trade_market");

	auto tradeApiMgr = TradeApiManager::instance();
	_tradeApi = tradeApiMgr->tradeApi();

	_symMgr = SymbolManager::instance();
	_balanceMgr = BalanceManager::instance();

	auto sh = _symMgr->getSymbolHandler(_symbol, _trade_market);
	_si = &sh->symbolInfo();
	sh->addListener(this);

	std::cout << "created CSampleStrategy:" << name() << std::endl;
}

void CSampleStrategy::notify(long id, bool force)
{
	std::cout << "CSampleStrategy notify " << id << " " << force << std::endl;
	Strategy::notify(id, force);

	// implement trading logic here
	if (false)
	{
		LOrder *ord = new LOrder();
		ord->sender = this;
		ord->account = 0;
		ord->use_margin = false;
		ord->side = BUY;
		ord->si = _si;
		ord->px = _si->getBestPrice(BUY);
		ord->qty = 0.001;
		ord->remainingQty = ord->qty;
		ord->signal = 0;
		ord->spread = _si->spread();
		ord->tif = Tif::TIF_IOC;
		ord->intention = OrderIntention::OI_AGGRESSIVE;
		ord->noticeId = _notMgr->getNoticeId();

		LOrder *order = _tradeApi->sendOrder(ord);
	}
}

void CSampleStrategy::onQuote(long cid, const SymbolInfo &si){
	std::cout<<"onQuote,bid_px :"<<_si->bid_px <<",ask_px :"<<_si->bid_px<<std::endl;
}	
void CSampleStrategy::onTick(long cid, const SymbolInfo &si){
	std::cout<<"onTick,bid_tbbo :"<<_si->bid_tbbo <<",ask_tbbo :"<<_si->ask_tbbo<<std::endl;
}	

void CSampleStrategy::onOrderCreated(LOrder* order){
	std::cout << " order created. orderId=" << order->orderId <<",status:" << "ADD"<<std::endl;
	
}	
void CSampleStrategy::onOrderAcked(LOrder* order){
	std::cout << " order acked. orderId=" << order->orderId <<",status:" << "ACK"<<std::endl;

}	
void CSampleStrategy::onOrderRejected(LOrder* order){
	std::cout << " order rejected. orderId=" << order->orderId <<",status:" << "RJD"<<std::endl;

}	
void CSampleStrategy::onOrderCancelCreated(LOrder* order){
	std::cout << " cancel order created. orderId=" << order->orderId <<",status:" << "CXL"<<std::endl;

}	
void CSampleStrategy::onOrderCancelAcked(LOrder* order){
	std::cout << " cancel order acked. orderId=" << order->orderId <<",status:" << "CAK"<<std::endl;

}	
void CSampleStrategy::onOrderCancelRejected(LOrder* order){
	std::cout << " cancel order rejected. orderId=" << order->orderId <<",status:" << "CRD"<<std::endl;

}	
void CSampleStrategy::onOrderExec(TradeDirs side, double px, double qty, Liquidity liq, LOrder* order){
	std::cout << " order exed. orderId=" << order->orderId <<",status:" << "EXE"<<std::endl;

}	
void CSampleStrategy::onOrderCanceled(LOrder* order){
	std::cout << "  order canceled. orderId=" << order->orderId <<",status:" << "CXD"<<std::endl;

}	
void CSampleStrategy::onOrderClosed(LOrder* order){
	std::cout << " order closed. orderId=" << order->orderId <<",status:" << "CLD"<<std::endl;

}	
