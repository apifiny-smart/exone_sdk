#pragma once
#include "base/Strategy.h"
#include "base/Order.h"
#include "components/NotificationManager.h"
#include "components/SymbolManager.h"
#include "components/BalanceManager.h"
#include "components/TradeApiManager.h"

class CSampleStrategy :public Strategy , public OrderSender 
{
public:
	using Strategy::Strategy;
	virtual ~CSampleStrategy(){};


	

	//jsonConfigurable interface
	/**
	 * @brief Initialization related parameters and other information 
	 * 
	 */
	void init() override;
	/**
	 * @brief 
	 * 
	 * @param id 
	 * @param force 
	 */
	void notify(long id, bool force=false) override;

	// strategy interface
	/**
	 * @brief Get the Symbol Info object
	 * 
	 * @return SymbolInfo* 
	 */
	SymbolInfo *getSymbolInfo() override { return _si; }
	/**
	 * @brief Callback triggered after receiving the command 
	 * 
	 * @param cmd 
	 */
	void onCommand(json &cmd) override { ; }
	/**
	 * @brief Get the Trade Market object
	 * 
	 * @return string& 
	 */
	string& getTradeMarket() { return _trade_market; }	
	/**
	 * @brief   Whether it is margin trading 
	 * 
	 * @return true 
	 * @return false 
	 */
	bool isUseMargin() { return _use_margin; }
	/**
	 * @brief Is it a cross margin mode or an isolated margin mode 
	 * 
	 * @return string& 
	 */
	string& marginSource() { return _margin_source; }

	//SymbolUpdateListener interface
	/**
	 * @brief Callback triggered by symbol update
	 * 
	 */
	void onSymbolUpdate() override {}

	// SymbolListener interface
	/**
	 * @brief top-Orderbook-triggered callbacks 
	 * 
	 * @param cid 
	 * @param si 
	 */
	void onQuote(long cid, const SymbolInfo &si) override;
	/**
	 * @brief Tick-by-tick-triggered callbacks 
	 * 
	 * @param cid 
	 * @param si 
	 */
	void onTick(long cid, const SymbolInfo &si) override;
	/**
	 * @brief Callback triggered by each deep market change 
	 * 
	 * @param cid 
	 * @param si 
	 */
	void onQuoteStale(long cid, const SymbolInfo &si) override {;}

	// TimerListener interface
	/**
	 * @brief timed callback
	 * 
	 * @param th 
	 * @param msecs 
	 */
	void onTimer(TimerHandler *th, long msecs) override{;}
	/**
	 * @brief 
	 * 
	 */
	void onNotify() override{;}

	// OrderSender interface
	/**
	 * @brief Callback triggered when order is created 
	 * 
	 * @param order 
	 */
	void onOrderCreated(LOrder *order) override;
	/**
	 * @brief Callback triggered when order is confirmed
	 * 
	 * @param order 
	 */
	void onOrderAcked(LOrder *order) override;
	/**
	 * @brief Callback triggered when order is rejected
	 * 
	 * @param order 
	 */
	void onOrderRejected(LOrder *order) override;
	/**
	 * @brief Callback triggered when cancelorder is created 
	 * @param order 
	 */
	void onOrderCancelCreated(LOrder *order) override;
	/**
	 * @brief Callback triggered when cancelorder is confirmed 
	 * 
	 * @param order 
	 */
	void onOrderCancelAcked(LOrder *order) override;
	/**
	 * @brief Callback triggered when cancelorder is rejected 
	 * 
	 * @param order 
	 */
	void onOrderCancelRejected(LOrder *order) override;
	/**
	 * @brief Callback triggered when order is executed
	 * 
	 * @param side 
	 * @param px 
	 * @param qty 
	 * @param liq 
	 * @param order 
	 */
	void onOrderExec(TradeDirs side, double px, double qty, Liquidity liq, LOrder *order) override;
	/**
	 * @brief Callback triggered when order is canceled
	 * 
	 * @param order 
	 */
	void onOrderCanceled(LOrder *order) override;
	/**
	 * @brief Callback triggered when order is closed
	 * 
	 * @param order 
	 */
	void onOrderClosed(LOrder *order) override;

protected:
	NotificationManager* _notMgr;
	TradeApi* _tradeApi;
	double _quote_spread;
	double _max_quote_error;

	std::string _symbol;
	std::string _trade_market;
	bool _use_margin = false;
	std::string _margin_source;
	SymbolManager *_symMgr = nullptr;
	SymbolInfo *_si = nullptr;

	BalanceManager* _balanceMgr = nullptr;	
};
