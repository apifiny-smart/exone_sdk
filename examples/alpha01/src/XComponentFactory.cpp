#include <boost/config.hpp> // for BOOST_SYMBOL_EXPORT
#include "base/XlibComponentFactory.h"
#include "strategies/CSampleStrategy.h"
#include "strategies/CTestStrategy.h"


namespace alpha01
{

    class Alpha01ComponentFactory : public XlibComponentFactory
    {
    public:
        Alpha01ComponentFactory()
        {
            std::cout << "Constructing Alpha01ComponentFactory xlib." << std::endl;
        }
        string getNameSpaceName()
        {
            return "Apifiny";
        }

        Sampler *getSampler(const string &type, const string &name, const json &attr)
        {
            return nullptr;
        }

        PricingModel *getPricingModel(const string &type, const string &name, const json &attr)
        {
            return nullptr;
        }

        Variable *getVariable(const string &type, const string &name, const json &attr)
        {
            return nullptr;
        }
        Dependant* getDependant(const string& type, const string& name, const json& attr){
            return nullptr;
        }

        Model* getModel(const string& type, const string& name, const json& attr){
            return nullptr;
        }

        Printer* getPrinter(const string& type, const string& name, const json& attr,int tradeDate){
            return nullptr;
        }

        Strategy *getStrategy(const string &type, const string &name, const json &attr)
        {
            std::cout<<"Alpha01ComponentFactory::getStrategy.\n";

            if (type == "CSampleStrategy")
                return new CSampleStrategy(name,attr);       
            if (type == "CTestStrategy")
                return new CTestStrategy(name,attr);         
            return nullptr;
        }
        
    };
    extern "C" BOOST_SYMBOL_EXPORT Alpha01ComponentFactory xCompFactory;
    Alpha01ComponentFactory xCompFactory;

} // namespace alpha01