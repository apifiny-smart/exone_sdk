#include "CCTrader.h"
#include "base/Order.h"
#include "components/TradeApiManager.h"
#include "components/SymbolManager.h"
#include "components/ConfigManager.h"

class MyStrategy : public OrderSender
{
public:
	using OrderSender::OrderSender;

	// implements OrderSender
	void onOrderCreated(LOrder *order) override
	{
		std::cout << "order is created id = " << order->remoteOrderId << std::endl;
	};
	void onOrderAcked(LOrder *order) override
	{
		std::cout << "order is ack id = " << order->remoteOrderId << std::endl;
	};
	void onOrderRejected(LOrder *order) override
	{
		std::cout << "order is rejected id = " << order->remoteOrderId << std::endl;
	};
	void onOrderCancelCreated(LOrder *order) override
	{
		std::cout << "order is cancel created id = " << order->orderId << std::endl;
	};
	void onOrderCancelAcked(LOrder *order) override
	{
		std::cout << "order is cancel ack id = " << order->remoteOrderId << std::endl;
	};
	void onOrderCancelRejected(LOrder *order) override
	{
		std::cout << "order is cancel rejected id = " << order->remoteOrderId << std::endl;
	};
	void onOrderExec(TradeDirs side, double px, double qty, Liquidity liq, LOrder *order) override
	{
		std::cout << "order is exec id = " << order->remoteOrderId << std::endl;
	};
	void onOrderCanceled(LOrder *order) override
	{
		std::cout << "order is canceled = " << order->remoteOrderId << std::endl;
	};
	void onOrderClosed(LOrder *order) override
	{
		std::cout << "order is closed id = " << order->remoteOrderId << std::endl;
	};
};

int main(int argc, char **argv)
{
	if (argc <= 1)
	{
		std::cout << "Usage: ccc_executor cfg_path" << std::endl;
		return 0;
	}

	// load configuration
	std::time_t t = std::time(0);
	std::tm *now = std::localtime(&t);
	int date = (now->tm_year + 1900) * 10000 + (now->tm_mon + 1) * 100 + now->tm_mday;
	json cfg = loadConfig(argv[1], date);
	json &instCfg = cfg["instance"];
	instCfg["tradeDate"] = date;
	instCfg["isLive"] = true;
	instCfg["listenAllSymbol"] = true;

	// setup logger
	string instName = getJsonValue(instCfg, "name", string("instname"));
	string logPath = getJsonValue(instCfg, "log_path", string("."));
	date = instCfg["tradeDate"];
	logPath += "/" + to_string(date);
	mkdirs(logPath);
	logPath += "/inst_" + instName + ".log";
	int logLevel = getJsonValue(instCfg, "log_level", qts::log4z::LOG_LEVEL_DEBUG);
	QTS_LOG_START(logLevel, logPath);

	// setup your custom trading env
	ConfigManager::instance()->setJsonCfg(cfg);
	auto tradeApiMgr = TradeApiManager::instance();
	auto symMgr = SymbolManager::instance();

	string symbol = "BTCUSDT";
	string exch = "BINANCEUS";
	auto sh = symMgr->getSymbolHandler(symbol, exch);
	auto si = &sh->symbolInfo();

	MyStrategy *strat = new MyStrategy();

	// start cctrader
	std::cout << "Starting trader ..." << std::endl;
	CCTrader client(cfg);
	client.initialize();
	client.async_run();

	TradeApi *tradeApi = tradeApiMgr->tradeApi(); // Must be called after initialize() function

	// start your trading loop
	sleep(10);
	while (true)
	{
		std::cout << "send a random IOC order" << std::endl;

		LOrder *ord = new LOrder();
		ord->sender = strat;
		ord->account = 101;
		ord->use_margin = true;
		ord->margin_source = "cross";
		ord->side = BUY;
		ord->si = si;
		ord->px = 39300.0;
		ord->qty = 0.001;
		ord->remainingQty = ord->qty;
		ord->signal = 0.0;
		ord->spread = si->spread();
		ord->tif = Tif::TIF_IOC;
		ord->intention = OrderIntention::OI_AGGRESSIVE;

		tradeApi->sendOrder(ord);

		sleep(60);
	}
}
