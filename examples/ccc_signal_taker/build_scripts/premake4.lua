#!lua

-- solution
solution "exone_examples"
   configurations { "Debug", "Release" }

project "ccc_signal_taker"
  kind "ConsoleApp"
  language "C++"
  includedirs {
      "../src",
      "../../../include",
      "/usr/local/include/decnumber",
      "/usr/local/include",
      "/usr/include"
  }
  files {
      "../src/**.cpp"
  }
  objdir "../../../build/obj"
  configuration "Debug"
      targetdir "../../../build/bin/debug"
      defines {"DEBUG", "USE_HDF5", "USE_QTS_LOG4","NO_PB"}
      flags {"Symbols"}
      libdirs {
          "../../../bin"
      }
      links {
          "hiredis","event", "event_pthreads","dl",
          "exone_trader","websockets",
          "boost_filesystem",
      }
      buildoptions {"-std=c++17", "-fPIC"}
      postbuildcommands { "cp -f ../../../build/bin/debug/ccc_signal_taker ../../../bin" }

  configuration "Release"
      targetdir "../../../build/bin/rel"
      defines {"NDEBUG", "USE_HDF5", "USE_QTS_LOG4","NO_PB"}
      flags {"Optimize"}
      libdirs {
          "../../../bin"
      }
      links {
          "exone_trader","websockets","hiredis",
          "boost_filesystem",
      }
      buildoptions {"-std=c++17", "-fPIC"}
      postbuildcommands { "cp -f ../../../build/bin/rel/ccc_signal_taker ../../../bin" }
    
