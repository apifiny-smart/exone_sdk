#pragma once
#include "base/Variable.h"

using namespace std;

class TrendVar: public Variable {
public:
	using Variable::Variable;
	void init() override;
	void onQuote(long cid, const SymbolInfo& si) override { setUpdated(); }
	void onTick(long cid, const SymbolInfo& si) override {}
	void onNotify() override;

private:
	SymbolInfo* _si;
	nanoseconds _dur;

	nanoseconds _lastSampleTime = nanoseconds(0);
	double _lastPx = 0.0;
};
