#include "TrendVar.h"
#include "components/TimerManager.h"
#include "components/SymbolManager.h"

void TrendVar::init() {
	auto symMgr = SymbolManager::instance();

	string ticker = _cfg.at("port").at(0);
	string exch = _cfg.at("port").at(1);
	auto sh = symMgr->getSymbolHandler(ticker, exch);
	sh->addListener(this);
	_si = &sh->symbolInfo();

	int dur = _cfg.at("dur");
	_dur = nanoseconds(dur);
}

void TrendVar::onNotify() {
	const nanoseconds& now = TimerManager::instance()->currentTime();
	if(now > _lastSampleTime + _dur) {
		double px = _si->mid_px();
		if(_lastPx > EPS) {
			_value = px - _lastPx;
			_ready = true;
		}
		else {
			_value = 0.0;
			_ready = false;
		}
		_lastSampleTime = now;
		_lastPx = px;		
	}
}
