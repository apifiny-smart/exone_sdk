#pragma once
#include "base/Strategy.h"
#include "components/TimerManager.h"
#include "base/Order.h"
#include "base/SymbolInfo.h"
#include "base/Variable.h"

class SignalTakeStrategy: public Strategy, public OrderSender {
public:
	using Strategy::Strategy;

    //implements Strategy
    void init() override;
	void onNotify() override;
	SymbolInfo *getSymbolInfo() override { return _si; }
	void onCommand(json& cmd) override {}
	bool isUseMargin() override { return _useMargin; }
	string& marginSource() override { return _marginSource; }

	//imlements SymbolListener
	void onQuote(long cid, const SymbolInfo& si) override;
	void onTick(long cid, const SymbolInfo& si) override;
	void onQuoteStale(long cid, const SymbolInfo& si) override {}

	//implements TimerListener
	void onTimer(TimerHandler* th, long msecs) override;

	//implements OrderSender
	void onOrderCreated(LOrder* order) override {};
	void onOrderAcked(LOrder* order) override {};
	void onOrderRejected(LOrder* order) override {};
	void onOrderCancelCreated(LOrder* order) override {};
	void onOrderCancelAcked(LOrder* order) override {};
	void onOrderCancelRejected(LOrder* order) override {};
	void onOrderExec(TradeDirs side, double px, double qty, Liquidity liq, LOrder* order) override {};
	void onOrderCanceled(LOrder* order) override {};
	void onOrderClosed(LOrder* order) override {};

protected:
	void sendIocOrder(TradeDirs side, double px, double qty);

protected:
	TimerManager *_timerMgr;
	SymbolInfo* _si;
	bool _useMargin = false;
	string _marginSource = "spot";
	bool _hasTimeEvt = false;

	Variable* _signalVar;
	double _order_size;
};
