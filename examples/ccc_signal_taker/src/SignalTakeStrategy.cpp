#include "SignalTakeStrategy.h"
#include "components/VariableManager.h"
#include "components/TradeApiManager.h"
#include "components/SymbolManager.h"

void SignalTakeStrategy::init() {
	auto symMgr = SymbolManager::instance();
	auto varMgr = VariableManager::instance();

	string symbol = _cfg.at("symbol");
	string exch = _cfg.at("trade_market");
	auto sh = symMgr->getSymbolHandler(symbol, exch);
    sh->addListener(this);
	_si = &sh->symbolInfo();

	_timerMgr = TimerManager::instance();
	TimerHandler* th = _timerMgr->getTimerHandler(60000);
	th->addListener(this);

	_order_size = _cfg.at("order_size");

	string signal = _cfg.at("signal");
	_signalVar = varMgr->getVariable(signal);
	addNotifiableChild(_signalVar);	
}

void SignalTakeStrategy::onNotify() {
    if(!_si->is_ready()) 
		return;
	
	if(!_signalVar->is_ready())
		return;

	if(_hasTimeEvt) {
		_hasTimeEvt = false;

		double midpx = _si->mid_px();
		double signal = _signalVar->value();

		if(signal > 0.01) {
			sendIocOrder(SELL, _si->bid_px, _order_size);
		} else if(signal < -0.01) {
			sendIocOrder(BUY, _si->ask_px, _order_size);
		}
	}
}

void SignalTakeStrategy::sendIocOrder(TradeDirs side, double px, double qty) {
	std::cout << "send IOC order: " << side << " " << px << " " << qty << std::endl;

	LOrder* ord = new LOrder();
	ord->sender = this;
	ord->account = 101;
	ord->use_margin = false;
	ord->margin_source = "spot";
	ord->side = side;
	ord->si = _si;
	ord->px = px;
	ord->qty = qty;
	ord->remainingQty = ord->qty;
	ord->signal = 0.0;
	ord->spread = _si->spread();
	ord->tif = Tif::TIF_IOC;
	ord->intention = OrderIntention::OI_AGGRESSIVE;

	TradeApi* tradeApi = TradeApiManager::instance()->tradeApi();
	tradeApi->sendOrder(ord);
}

void SignalTakeStrategy::onQuote(long cid, const SymbolInfo& si) {

}

void SignalTakeStrategy::onTick(long cid, const SymbolInfo& si) {

}

void SignalTakeStrategy::onTimer(TimerHandler* th, long msecs) {
	_hasTimeEvt = true;
}